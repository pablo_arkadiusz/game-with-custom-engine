#pragma once

#include <Mono_behaviour.hpp>
#include <Cube_component.hpp>
#include <Player_controller.hpp>
#include <Box_collider2d.hpp>
#include <math.hpp>
#include <Key.hpp>
#include <Audio_manager.hpp>
#include <functional>

using namespace engine;


class Player : public Mono_behaviour
{

private:
	Audio_manager& audio_manager; ///< Referencia al manager del audio

	// Componentes
private:
	Player_controller controller;
	Cube_component cube;
	Box_collider2d collider;

private:
	std::vector<Key*> keys; ///< Lista de keys que el jugador tiene recogidos
	float move_speed{ 1.5f }; ///< Velocidad de movimiento
	const Vector3 spawn_point{ -5.4, 0, 0 }; ///< Posicion donde el jugador spawnea
	bool move_left{ false }, move_right{ false }, move_up{ false }, move_down{ false }; // Booleanos que indican la direccion de movimiento

public:
	Player(Scene& s, Audio_manager& audio_manager, const std::string& name = "Player");

public:
	void try_take_key(Key& key); ///< Se intenta coger una llave (solo se podra coger si se tiene la del orden / numero anterior)

private:
	void add_components(); ///< Se anade todos los componentes (controlador, cubo y colisionador)
	void bind_input_keys(); ///< Se bindea los movimientos al Action Input
	void drop_all_keys(); ///< El jugador pierde todas las llaves acumuladas
	void respawn(); ///< El jugador respawnea
	void manage_movement(float delta_time); ///< Se maneja el movimiento del jugador

	// Metodos que ajustan el movimiento
public:
	inline void go_left() { move_left = true; }
	inline void stop_left() { move_left = false; }
	inline void go_rigth() { move_right = true; }
	inline void stop_right() { move_right = false; }
	inline void go_up() { move_up = true; }
	inline void stop_up() { move_up = false; }
	inline void go_down() { move_down = true; }
	inline void stop_down() { move_down = false; }

private:
	void update(float delta_time) override;
	void on_trigger_enter(Game_object& other) override;
};