#pragma once


#include <Wall.hpp>
#include <Mono_behaviour.hpp>

using namespace engine;

/// Molino que rota sobre si mismo compuesto de varias paredes
class Windmill : public Mono_behaviour
{
	// Paredes que forman el molino
private:
	Wall wall1{ Wall(scene) }, wall2{ Wall(scene) }, wall3{ Wall(scene) };
	
	// Variables
private:
	float rot_speed{ 0.65f }; ///< Velocidad de rotacion
	float size{ 5 }; ///< Longitud de las astas del molino

private:
	void manage_rotation(float delta_time); ///< Maneja la rotacion del molino

public:
	Windmill(Scene& s, std::string name = "Windmill");

public:
	void set_position(vec3 pos);
	void update(float delta_time) override;

};

