#pragma once

#include <Mono_behaviour.hpp>
#include <Box_collider2d.hpp>
#include <Cube_component.hpp>
#include <Audio_manager.hpp>

using namespace engine;

class Exit_door : public Mono_behaviour
{

private:
	Audio_manager &audio_manager;
private:
	Cube_component cube;
	Box_collider2d collider;

private:
	float move_speed{ 1 };
	bool is_open{ false };

public:
	Exit_door(Scene& s, Audio_manager& audio_manager, std::string name = "Exit Door") : audio_manager{ audio_manager }, Mono_behaviour(s, name)
	{
		game_object.add_component(cube);
		game_object.add_component(collider);
		collider.only_passive = true;

		// Ajustes de transform de la puerta
		transform.scale(Vector3(0.02, 0.45, 0.0001));
		transform.set_pos(Vector3(5, 0, 0));

		game_object.add_tag("Wall");
	}

private:
	void manage_movement(float delta_time)
	{
		// La puerta seaparta una vez que ha sido abierta
		if (is_open) transform.translate(Vector3(delta_time * move_speed, 0, 0));
	}

public:
	void open() 
	{
		is_open = true; 
		audio_manager.play_open_door();
	}

	void close()
	{
		is_open = false;
		transform.set_pos(Vector3(5, 0, 0));
	}

public:
	void update(float delta_time) override
	{
		manage_movement(delta_time);
	}

};

