#include "Windmill.hpp"

void Windmill::manage_rotation(float delta_time)
{
	const float rotation_amount{ rot_speed * delta_time };
	wall1.get_transform().rotate(vec3(0, 0, rotation_amount));
	wall2.get_transform().rotate(vec3(0, 0, rotation_amount));
	wall3.get_transform().rotate(vec3(0, 0, rotation_amount));
}

Windmill::Windmill(Scene& s, std::string name) : Mono_behaviour(s, name)
{
	// Se crean los ajustes del transform de las paredes que forman el molino

	wall1.get_transform().scale(vec3(size, 0.1f, 0.0001f));
	wall2.get_transform().scale(vec3(size, 0.1f, 0.0001f));
	wall3.get_transform().scale(vec3(size, 0.1f, 0.0001f));
	wall1.get_transform().rotate(vec3(0, 0, 60 * 3.14 / 180));
	wall2.get_transform().rotate(vec3(0, 0, 120 * 3.14 / 180));
}

void Windmill::set_position(vec3 pos)
{
	transform.set_pos(pos);
	wall1.get_transform().set_pos(pos);
	wall2.get_transform().set_pos(pos);
	wall3.get_transform().set_pos(pos);
}

void Windmill::update(float delta_time)
{
	manage_rotation(delta_time);
}
