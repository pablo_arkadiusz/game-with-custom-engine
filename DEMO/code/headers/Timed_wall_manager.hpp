#pragma once

#include <array>
#include <Mono_behaviour.hpp>

using namespace engine;

class Timed_wall;

/// Manager de las paredes del nivel 1
class Timed_wall_managerL1 : public Mono_behaviour
{
	
public:
	Timed_wall_managerL1(Scene& s) : Mono_behaviour(s, "Timed Wall Manager"){}

public:
	void load_walls(std::array<Timed_wall, 4> &walls);

private:
	/// Array con las cuatro walls que maneja
	std::array<Timed_wall*, 4> walls_arr{nullptr, nullptr, nullptr, nullptr};

	float elapsed_time{ 0.f }; ///< Tiempo transcurrido desde el comienzo
	float init_time{ 0.7f }; ///< Tiempo cada cuanto inicializa un wall
	int wall_index{ 0 }; ///< Indice de la pared que tiene que inicializar

private:
	void init_walls(float delta_time); ///< Inicializa las paredes segun el tiempo transcurrido

public:
	void update(float delta_time) override;
	
};

