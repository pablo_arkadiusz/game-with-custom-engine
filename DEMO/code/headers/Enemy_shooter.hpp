#pragma once


#include <Mono_behaviour.hpp>
#include <Scene.hpp>
#include <Graphic_component.hpp>
#include <Cube_component.hpp>
#include <Bullet.hpp>
#include <math.h>
#include <memory>
#include <Object_pool.hpp>
#include <Bullet.hpp>

using namespace engine;



class Enemy_shooter : public Mono_behaviour
{
private: 
	using bullet_ptr_pair = std::pair< std::shared_ptr<Bullet>, std::shared_ptr<Bullet>>;

private:
	Cube_component graphic;

private:
	const size_t bullet_amount{ 26 }; ///< Numero de balas que se instanciara

	Object_pool<Bullet> bullet_pool{ bullet_amount }; ///< Memory Pool para las balas

	int bullet_index{ 0 }; ///< Indice para acceder a la siguiente bala del pool para disparar
	float elapsed_time_from_last_shoot{ 0 }; ///< Tiempro transcurrido desde el ultimo disparo
	float cooldown{ 1.f }; ///< Tiempo entre cada disparo
	std::vector<std::shared_ptr<Bullet>> bullet_ptr_list; ///< Vector con todas las balas.
	float rotation_speed{ 2 }; ///< Velocidad de rotacion del shooter

public:
	Enemy_shooter(Scene& s, const size_t bullets = 26, std::string name = "Enemy Shooter");
	~Enemy_shooter() = default;
	
public:
	/// Ajusta el tiempo de cooldown
	void set_cd(float new_cd) { cooldown = new_cd; }
	/// Ajusta la velocidad de rotacion
	void set_rot(float new_r) { rotation_speed = new_r ; }

private:
	void create_bullets(); ///< Se crean las balas utilizando el Object Pool de balas

private:
	bullet_ptr_pair get_two_bullets(); ///< Devuelve las dos siguientes balas disponibles
	/// Se asegura que las dos balas vayan en sentido opuesto
	void manage_bullet_directions(std::shared_ptr<Bullet> b1_ptr, std::shared_ptr<Bullet> b2_ptr); 
	void shoot(); ///< Dispara dos balas en sentido contrario
	void try_shoot(float delta_time); ///< Maneja la logica de los disparos
	void manage_rotation(float delta_time); ///< Rota el objeto segun su velocidad de rotacion

private:
	void update(float delta_time) override
	{	
		manage_rotation(delta_time);
		try_shoot(delta_time);
	}
};