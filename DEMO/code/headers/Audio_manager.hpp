#pragma once

#include <Audio_source.hpp>
#include <Mono_behaviour.hpp>
#include <Audio_player.hpp>
using namespace engine;

/// Manager encargado de cargar y reproducir los audios de un nivel
class Audio_manager : public Mono_behaviour
{

private:
	Audio_source audio_component;
public:
	Audio_manager(Scene& scene);

public:
	~Audio_manager() {}
private:
	void load_audio_files();

	// Pistas de musica
private:
	Mix_Chunk* bad_pickup_sound{ nullptr }; ///< Audio de recogida de objeto fallida
	Mix_Chunk* pickup_sound{ nullptr }; ///< Audio de recogida de objeto
	Mix_Chunk* death_sound{ nullptr }; ///< Audio de muerte
	Mix_Chunk* open_door_sound{ nullptr }; ///< Audio de abrir puerta
	_Mix_Music* background_music{ nullptr }; ///< Musica de background
	
public:
	void play_death(); ///< Se reproduce el sonido de muerte
	void play_pickup(); ///< Se reproduce el sonido de recogida de llave
	void play_bad_pickup(); ///< Se reproduce el sonido de tratar de recoger una llave que aun no se puede
	void play_open_door(); ///< Se reproduce sonido de abrir la puerta de salida
};

