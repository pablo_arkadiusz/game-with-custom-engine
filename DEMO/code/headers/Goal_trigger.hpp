#pragma once

#include <Mono_behaviour.hpp>
#include <Scene.hpp>
#include <map>
#include <Box_collider2d.hpp>

using namespace engine;

/// Trigger que acaba la escena cuando lo toca el jugador
class Goal_trigger : public Mono_behaviour
{
private:
	Box_collider2d col;

public:
	Goal_trigger(Scene& s, const std::string& name = "Goal Trigger");

private:
	void on_trigger_enter(Game_object& other) override;
};

