#pragma once



#include <Mono_behaviour.hpp>
#include <array>

using namespace engine;

class Timed_wall;

/// Manager de las paredes del nivel 2
class Timed_wall_managerL2 : public Mono_behaviour
{
	// Variables de ajustes para las paredes
	const float cooldown{ 0.1f }, speed{ 4.f }, rotation_speed{ 20.f };

public:
	Timed_wall_managerL2(Scene& s, std::string name = "Timed Wall Manager") : Mono_behaviour(s, name) 
	{
		enabled = false;
	}

private:
	/// Array con las 7 paredes que el manager controlara / inicializara
	std::array<Timed_wall*, 7> walls_arr{ nullptr,nullptr ,nullptr ,nullptr ,nullptr ,nullptr ,nullptr };

public:
	void load_walls(std::array<Timed_wall, 7> &walls);

};

