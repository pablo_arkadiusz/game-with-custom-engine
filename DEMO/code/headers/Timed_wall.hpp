#pragma once

#include <Game_object.hpp>
#include <Cube_component.hpp>
#include <Box_collider2d.hpp>
#include <Timed_wall_manager.hpp>
#include <Wall_trigger.hpp>
#include <Scene.hpp>
#include <stdlib.h>  /* abs */

using namespace engine;

class Timed_wall : public Mono_behaviour
{
	// Componentes
private:
	Cube_component cube;
	Box_collider2d collider;

	// Variables
private:
	bool initialized{ false }; ///< True si se han inicializado todos los ajustes previos
	float speed{ 5 }; ///< Velocidad de movimiento
	vec2 direction{ 0, -1 }; ///< Direccion de movimiento
	float stop_time{ 1.2f }; ///< Tiempo que estara quiero al tocar una pared
	float elapsed_stop_time{ 0 }; ///< Tiempo transcurrido desde el contacto con una pared (tiempo que lleva quieto)
	bool moving{ false };  ///< True si se esta moviento. False si esta parado
	float rotation_speed{ 0 }; ///< Velocidad de rotacion
	bool use_limit_walls = true; // True si colisiona con las paredes del limite del nivel. False si las debe ignorar (como en el caso de la serpiente del nivel 4 por ejemplo)

	// Metodos

public:
	Timed_wall(Scene& s);

	// Setters
public:
	inline void set_rotation(float r) { rotation_speed = r; }
	inline void set_cooldown(float cd) { stop_time = cd; }
	inline void set_speed(float s) { speed = s; }

public:
	inline void init() { initialized = true; }
	inline void launch() { moving = true; }
	inline void change_y_direction()  { direction.y *= -1; }

	/// El objeto ignorara las colisiones con la pared limite
	inline void ignore_limit_walls() { use_limit_walls = false; }

	// Manejo de la direccion del movimiento
	inline void move_right() { direction = vec2(1, 0); }
	inline void move_up() { direction = vec2(0, 1); }
	inline void move_left() { direction = vec2(-1, 0); }
	inline void move_down() { direction = vec2(0, -1); }

private:
	inline void stop() { moving = false; }
	void move(float delta_time); ///< Se mueve el objeto segun su velocidad y direccion
	void cooldown(float delta_time); ///< Se carga el cooldown de parada al tocar una pared
	void manage_movement(float delta_time); ///< Maneja la logica de movimiento
	void manage_rotation(float delta_time); ///< Maneja la logica de la rotacion

private:
	void start() override;
	void update(float delta_time) override;
	void on_trigger_enter(Game_object& other) override;
};
