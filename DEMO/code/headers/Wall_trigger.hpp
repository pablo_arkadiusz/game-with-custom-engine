#pragma once



#include <Box_collider2d.hpp>
#include <Cube_component.hpp>
#include <Mono_behaviour.hpp>

using namespace engine;

/// Pared que se usa como trigger para Timed Walls del nivel 4 (serpientes)
class Wall_trigger : public Mono_behaviour
{

private:
	glt::Vector3 collision_point; ///< Punto de referencia de colision. Para que siempre sea en el mismo sitio a pesar de que cambie el frame rate

private:
	Box_collider2d collider;
	//Cube_component cube_component;

public:
	Wall_trigger(Scene& s, const std::string& name = "Wall Trigger") : Mono_behaviour(s, name)
	{
		game_object.add_component(collider);
		enabled = false; // Se desactiva update y start
		//game_object.add_component(cube_component);
	}

public:
	/// Devuelve el punto de colision que deberia tener si todos las colisiones fueran con el mismo frame
	glt::Vector3 get_collision_point() { return collision_point; }

private:
	void on_trigger_enter(Game_object& other) override
	{
		if (other.check_tags({ "Timed Wall" }))
		{
			// La primera colision de todas sera el punto de referencia del punto de colision
			collision_point = other.get_transform().get_position();
			collider.only_passive = true; // Se desactiva este callback
		}
	}

};

