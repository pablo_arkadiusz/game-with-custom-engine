#pragma once


#include <Mono_behaviour.hpp>
#include <Graphic_component.hpp>
#include <Circle_collider2d.hpp>
#include <Cube_component.hpp>

using namespace engine;

/// Bala que avanza en una direccion con una determinada velocidad
class Bullet : public Mono_behaviour
{
private:
	Circle_collider2d collider;
	Cube_component graphic;

private:
	bool run{ false }; ///< Si es true se movera con la velocidad y direccion que tenga
	Vector2 direction; ///< direccion de movimiento
	float speed{ 1 }; ///< Velocidad global que se aplicara a cada direccion

public:
	float get_speed() const { return speed; }

public:
	Bullet(Scene& s);

public:
	void shoot(const vec3 &pos, const vec2 &direction); ///< Lanza la bala desde una posicion con la direccion indicada
	void change_direcion(); ///< Cambia la direccion de la bala al otro lado (180 grados)

private:
	void movement(float delta_time); ///< Logica de movimiento de la bala
private:
	void update(float delta_time) override;

};

