#pragma once

#include <Mono_behaviour.hpp>
#include <Mesh_component.hpp>
#include <Circle_collider2d.hpp>
#include <Exit_door.hpp>

using namespace engine;

class Key : public Mono_behaviour
{

private:
	size_t number; ///< Prioridad o numero de orden segun el cual hay que recoger la llave / numero
	float rotation_speed{ 3 }; ///< Velocidad de rotacion
	Exit_door* door_ptr{ nullptr }; ///< Puntero a la puerta que abre. Solo la ultima llave del nivel abre una puerta, si no sera nullptr
	Transform init_tranform; ///< Ajustes de transform al comienzo de la partida (Para volver cuando el jugador pierda la llave)

	// Componentes
private:
	Circle_collider2d collider;
	Mesh_component cube_component;

public:
	Key(Scene& s, size_t num);

public:
	/// Asigna el puntero a la puerta que se abrira al recoger esta llave
	void assign_door(Exit_door* exit_door) { door_ptr = exit_door; }

	/// Comprueba el numero de orden de la llave
	inline size_t get_number() { return number; }

	void take(); ///< La llave es cogida por el jugador
	void drop(); ///< La llave es soltada por el jugador

private:
	void start() override
	{
		init_tranform = transform;
	}

	void update(float delta_time) override
	{		
		// Se rota la llave
		transform.rotate(Vector3(0, 0, delta_time * rotation_speed));
	}
};

