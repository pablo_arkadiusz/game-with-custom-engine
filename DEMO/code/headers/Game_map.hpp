#pragma once


#include <Game_object.hpp>
#include <Wall.hpp>
#include <Camera_object.hpp>
#include <Light_object.hpp>
#include <Player.hpp>
#include <Goal_trigger.hpp>
#include <Audio_manager.hpp>


/// Parte del mapa del juego que es comun para todas las escenas
class Game_static_map : public Game_object
{

public:
	Game_static_map(Scene& s, const std::string& name = "Game Map") : Game_object(s, name), audio_manager{ s }
	{
		transform_walls();
	}

	// Objetos que componen el mapa
private:
	Goal_trigger goal{ Goal_trigger(scene) };
	Wall top_limit{ Wall(scene) }, bottom_limit{ Wall(scene) },
		right_limit{ Wall(scene) }, left_limit{ Wall(scene) };
	Wall spawn_wall1{ Wall(scene) }, spawn_wall2{ Wall(scene) }, exit_wall1{ Wall(scene) }, exit_wall2{ Wall(scene) };
	Camera_object camera{ Camera_object(scene) };
	Light_object light{ Light_object(scene) };
	Audio_manager audio_manager{ scene };
	Exit_door exit_door{ Exit_door(scene, audio_manager) };
	Player player{ scene, audio_manager };

private:
		
	void transform_walls(); ///< Se crean los ajustes de los objetos

};

