#pragma once

#include <Game_object.hpp>
#include <Scene.hpp>
#include <Exit_door.hpp>
#include <Key.hpp>
#include <Timed_wall.hpp>
#include <Timed_wall_manager.hpp>
#include <Wall_trigger.hpp>
#include <array>

using namespace engine;

/// Objetos del nivel 4
class Level_Four_objects : public Game_object
{

public:
	Level_Four_objects(Scene& s, const std::string& name = "Level One Parent") : Game_object(s, name)
	{
		init_objects();
	}

private:
	Key key1{ Key(scene, 1) };

	// Paredes limite de cambio de direccion para cada una de las serpientes
	Wall_trigger trigger_left1{ Wall_trigger(scene) }, trigger_right1{ Wall_trigger(scene) }, trigger_up1{ Wall_trigger(scene) }, trigger_down1{ Wall_trigger(scene) };
	Wall_trigger trigger_left2{ Wall_trigger(scene) }, trigger_right2{ Wall_trigger(scene) }, trigger_up2{ Wall_trigger(scene) }, trigger_down2{ Wall_trigger(scene) };
	Wall_trigger trigger_left3{ Wall_trigger(scene) }, trigger_right3{ Wall_trigger(scene) }, trigger_up3{ Wall_trigger(scene) }, trigger_down3{ Wall_trigger(scene) };
	Wall_trigger trigger_left4{ Wall_trigger(scene) }, trigger_right4{ Wall_trigger(scene) }, trigger_up4{ Wall_trigger(scene) }, trigger_down4{ Wall_trigger(scene) };

	// Esta escena esta hardcodeada temporalmente. TODO: Encapsularlo creando una clase Snake

	// Primera serpiente
	std::array<Timed_wall, 33> snake1
	{
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene)
	};

	// Segunda serpiente
	std::array<Timed_wall, 25> snake2
	{
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene)
	};

	// Tercera serpiente
	std::array<Timed_wall, 15> snake3
	{
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene)
	};

	// Cuarta serpiente
	std::array<Timed_wall, 5> snake4
	{
		Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene),Timed_wall(scene)
	};

private:
	void init_objects();


};

