#include "Goal_trigger.hpp"

Goal_trigger::Goal_trigger(Scene& s, const std::string& name) : Mono_behaviour(s, name)
{
	game_object.add_component(col);	

	transform.scale(Vector3(0.1, 0.6, 0.0001));
	transform.translate(Vector3(5.87, 0, 0));

	enabled = false; // Desabilita los metodos update y start
}

void Goal_trigger::on_trigger_enter(Game_object& other)
{
	// Si se toca la meta. La escena acaba
	if (other.check_tags({ "Player" }))
		scene.stop();
}
