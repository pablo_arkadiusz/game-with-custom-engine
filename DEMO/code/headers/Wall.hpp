#pragma once


#include <Graphic_component.hpp>
#include <Box_collider2d.hpp>
#include <Cube_component.hpp>
#include <Mono_behaviour.hpp>

using namespace engine;

/// Simple pared estatica
class Wall : public Mono_behaviour
{

private:
	Cube_component graphic;
	Box_collider2d collider;

public:
	Wall(Scene& s, std::string name = "Wall") : Mono_behaviour(s, name)
	{
		game_object.add_component(graphic);
		game_object.add_component(collider);
		collider.only_passive = true;
		game_object.add_tag("Wall");

		enabled = false; // Desabilita los metodos update y start
	}

};

