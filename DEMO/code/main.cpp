/*
 * Author: Pablo Arkadiusz Kalemba
 * Date: January 2021
 */

#include <Window.hpp>
#include <Game_map.hpp>
#include <Level_four_objects.hpp>
#include <Timed_wall_manager.hpp>
#include <Timed_wall_managerL2.hpp>
#include <Windmill.hpp>
#include <Enemy_shooter.hpp>
#include <Scene_director.hpp>

using namespace engine;

void play_level_one(Scene &s);
void play_level_two(Scene& s);
void play_level_three(Scene& s);
void play_level_four(Scene& s);
void play_level_five(Scene& s);


int main()
{
	Window window("TEST", 1280, 720, false); // Se crea la ventana del juego
	window.set_title("Worlds Hardest Game");
	Scene_director &director{ Scene_director::get_instance() };
	
	if (director.can_play())
	{
		Scene scene1(window);
		play_level_one(scene1);
	}
	
	if (director.can_play())
	{
		Scene scene2(window);
		play_level_two(scene2);
	}

	if (director.can_play())
	{
		Scene scene3(window);
		play_level_three(scene3);
	}

	if (director.can_play())
	{
		Scene scene4(window);
		play_level_four(scene4);
	}
	
	if (director.can_play())
	{
		Scene scene5(window);
		play_level_five(scene5);
	}
	
	return 0;
}


void play_level_one(Scene& scene)
{
	// Static map
	Audio_manager audio_manager(scene);
	Player player(scene, audio_manager);
	Exit_door exit_door{ Exit_door(scene, audio_manager) };
	Goal_trigger goal{ Goal_trigger(scene) };
	Wall top_limit{ Wall(scene) }, bottom_limit{ Wall(scene) }, right_limit{ Wall(scene) }, left_limit{ Wall(scene) };
	Wall spawn_wall1{ Wall(scene) }, spawn_wall2{ Wall(scene) }, exit_wall1{ Wall(scene) }, exit_wall2{ Wall(scene) };

	//Level one objects
	Key key1{ Key(scene, 1) };
	std::array<Timed_wall, 4> timed_walls { Timed_wall(scene), Timed_wall(scene), Timed_wall(scene), Timed_wall(scene) };
	Timed_wall_managerL1 wall_manager{ Timed_wall_managerL1(scene) };

	// Inicializacion de los ajustes de algunos objetos
	wall_manager.load_walls(timed_walls);
	key1.assign_door(&exit_door);
	top_limit.get_gameobject().add_tag("Edge Wall"); bottom_limit.get_gameobject().add_tag("Edge Wall");
	right_limit.get_gameobject().add_tag("Edge Wall"); left_limit.get_gameobject().add_tag("Edge Wall");

	// Creacion de mapa para identificacion con el archivo xml
	std::unordered_map<std::string, Mono_behaviour*> mono_behaviour_map
	{
		{ "Player",  &player },
		{ "Top_limit", &top_limit }, { "Bottom_limit", &bottom_limit }, { "Right_limit", &right_limit }, { "Left_limit", &left_limit },
		{ "Top_spawn_wall", &spawn_wall1 }, { "Bottom_spawn_wall", &spawn_wall2 }, { "Top_exit_wall", &exit_wall1 }, { "Bottom_exit_wall", &exit_wall2 },
		{ "Timed_wall1", &timed_walls[0] }, { "Timed_wall2", &timed_walls[1]}, { "Timed_wall3", &timed_walls[2] },{ "Timed_wall4", &timed_walls[3]},
		{ "Goal_trigger", &goal },
		{ "Exit_door", &exit_door },
		{ "Key1", &key1 },
		{ "Wall_manager", &wall_manager}
	};

	scene.load_scene_xml("../../../DEMO/scenes/scene1.xml", mono_behaviour_map);
	scene.load_input_xml("../../../DEMO/input/input-mapping.xml");
	scene.run();
}

void play_level_two(Scene& scene)
{
	// Static map
	Audio_manager audio_manager(scene);
	Player player(scene, audio_manager);
	Exit_door exit_door{ Exit_door(scene, audio_manager) };
	Goal_trigger goal{ Goal_trigger(scene) };
	Wall top_limit{ Wall(scene) }, bottom_limit{ Wall(scene) }, right_limit{ Wall(scene) }, left_limit{ Wall(scene) };
	Wall spawn_wall1{ Wall(scene) }, spawn_wall2{ Wall(scene) }, exit_wall1{ Wall(scene) }, exit_wall2{ Wall(scene) };

	// Objetos del nivel 2
	Key key1{ Key(scene, 1) }, key2{ Key(scene, 2) };
	std::array<Timed_wall, 7> timed_walls
	{
		Timed_wall(scene), Timed_wall(scene), Timed_wall(scene),
		Timed_wall(scene), Timed_wall(scene), Timed_wall(scene),
		Timed_wall(scene)
	};

	Timed_wall_managerL2 wall_manager{ Timed_wall_managerL2(scene) };

	// Configuraciones de los objetos
	wall_manager.load_walls(timed_walls);
	key2.assign_door(&exit_door);
	top_limit.get_gameobject().add_tag("Edge Wall"); bottom_limit.get_gameobject().add_tag("Edge Wall");
	right_limit.get_gameobject().add_tag("Edge Wall"); left_limit.get_gameobject().add_tag("Edge Wall");

	// Creacion de mapa para identificacion con el archivo xml
	std::unordered_map<std::string, Mono_behaviour*> mono_behaviour_map
	{
		{ "Player",  &player },
		{ "Top_limit", &top_limit }, { "Bottom_limit", &bottom_limit }, { "Right_limit", &right_limit }, { "Left_limit", &left_limit },
		{ "Top_spawn_wall", &spawn_wall1 }, { "Bottom_spawn_wall", &spawn_wall2 }, { "Top_exit_wall", &exit_wall1 }, { "Bottom_exit_wall", &exit_wall2 },
		{ "Goal_trigger", &goal },
		{ "Exit_door", &exit_door },
		{ "Key1", &key1 }, { "Key2", &key2 },
		{ "Timed_wall1", &timed_walls[0] }, { "Timed_wall2", &timed_walls[1]}, { "Timed_wall3", &timed_walls[2] },{ "Timed_wall4", &timed_walls[3]},
		{ "Timed_wall5", &timed_walls[4] }, { "Timed_wall6", &timed_walls[5]}, { "Timed_wall7", &timed_walls[6] },
		{ "Wall_manager", &wall_manager}
	};

	scene.load_scene_xml("../../../DEMO/scenes/scene2.xml", mono_behaviour_map);
	scene.load_input_xml("../../../DEMO/input/input-mapping.xml");

	scene.run();

}

void play_level_three(Scene& scene)
{
	// Static map
	Audio_manager audio_manager(scene);
	Player player(scene, audio_manager);
	Exit_door exit_door{ Exit_door(scene, audio_manager) };
	Goal_trigger goal{ Goal_trigger(scene) };
	Wall top_limit{ Wall(scene) }, bottom_limit{ Wall(scene) }, right_limit{ Wall(scene) }, left_limit{ Wall(scene) };
	Wall spawn_wall1{ Wall(scene) }, spawn_wall2{ Wall(scene) }, exit_wall1{ Wall(scene) }, exit_wall2{ Wall(scene) };

	// Objetos de nivel 3
	Key key1{ Key(scene, 1) }, key2{ Key(scene, 2) };
	Windmill wind_mill1{ Windmill(scene) }, wind_mill2{ Windmill(scene) };

	// Configuraciones de los objetos
	key2.assign_door(&exit_door);

	// Creacion de mapa para identificacion con el archivo xml
	std::unordered_map<std::string, Mono_behaviour*> mono_behaviour_map
	{
		{ "Player",  &player },
		{ "Top_limit", &top_limit }, { "Bottom_limit", &bottom_limit }, { "Right_limit", &right_limit }, { "Left_limit", &left_limit },
		{ "Top_spawn_wall", &spawn_wall1 }, { "Bottom_spawn_wall", &spawn_wall2 }, { "Top_exit_wall", &exit_wall1 }, { "Bottom_exit_wall", &exit_wall2 },
		{ "Goal_trigger", &goal },
		{ "Exit_door", &exit_door },
		{ "Key1", &key1 }, { "Key2", &key2 },
		{ "Windmill1", &wind_mill1 }, { "Windmill2", &wind_mill2 }
	};

	scene.load_scene_xml("../../../DEMO/scenes/scene3.xml", mono_behaviour_map);
	scene.load_input_xml("../../../DEMO/input/input-mapping.xml");

	scene.run();
}

void play_level_four(Scene& scene)
{
	// Este nivel de momento no se carga desde el xml
	Game_static_map map(scene);
	Level_Four_objects parent_object(scene);
	scene.load_input_xml("../../../DEMO/input/input-mapping.xml");

	scene.run();
}

void play_level_five(Scene& scene)
{
	// Static map
	Audio_manager audio_manager(scene);

	Player player(scene, audio_manager);
	Exit_door exit_door{ Exit_door(scene, audio_manager) };
	Goal_trigger goal{ Goal_trigger(scene) };
	Wall top_limit{ Wall(scene) }, bottom_limit{ Wall(scene) }, right_limit{ Wall(scene) }, left_limit{ Wall(scene) };
	Wall spawn_wall1{ Wall(scene) }, spawn_wall2{ Wall(scene) }, exit_wall1{ Wall(scene) }, exit_wall2{ Wall(scene) };

	// Objetos del nivel 5
	Key key1{ Key(scene, 1) }, key2{ Key(scene, 2) };
	Enemy_shooter shooter1{ Enemy_shooter(scene, 20) }, shooter2{ Enemy_shooter(scene, 12) }, shooter3{ Enemy_shooter(scene, 12) }, shooter4{ Enemy_shooter(scene,12) }, shooter5{ Enemy_shooter(scene,12) };

	key2.assign_door(&exit_door);
	shooter1.set_cd(0.5f);
	shooter1.set_rot(1);

	// Creacion de mapa para identificacion con el archivo xml
	std::unordered_map<std::string, Mono_behaviour*> mono_behaviour_map
	{
		  { "Player",  &player },
		  { "Top_limit", &top_limit }, { "Bottom_limit", &bottom_limit }, { "Right_limit", &right_limit }, { "Left_limit", &left_limit },
		  { "Top_spawn_wall", &spawn_wall1 }, { "Bottom_spawn_wall", &spawn_wall2 }, { "Top_exit_wall", &exit_wall1 }, { "Bottom_exit_wall", &exit_wall2 },
		  { "Goal_trigger", &goal },
	      { "Exit_door", &exit_door },
		  { "Key1", &key1 }, { "Key2", &key2 },
		  { "Shooter1", &shooter1 }, { "Shooter2", &shooter2 }, { "Shooter3", &shooter3 }, { "Shooter4", &shooter4 }, { "Shooter5", &shooter5 }
	};

	scene.load_scene_xml("../../../DEMO/scenes/scene5.xml", mono_behaviour_map);
	scene.load_input_xml("../../../DEMO/input/input-mapping.xml");
	scene.run();
}