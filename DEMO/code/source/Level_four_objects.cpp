#include <Level_four_objects.hpp>



void Level_Four_objects::init_objects()
{
	// Esta escena esta hardcodeada temporalmente. TODO: Encapsularlo creando una clase Snake

	key1.assign_door(dynamic_cast<Exit_door*>(scene.find_gameobject("Exit Door")->get_mono_ptr()));

	float speed{ 1.5f };

	// Se inicializa la primera serpiente
	for (size_t i{ 0 }; i != snake1.size(); ++i)
	{
		snake1[i].get_transform().set_pos(vec3(-3.7, 1 + (0.75f * i), 0));
		//scene.print( std::to_string( wall_arr[i].get_transform().get_position().x ));
		snake1[i].get_transform().scale(vec3(0.25, 0.25, 0.0001));
		snake1[i].move_down();
		snake1[i].set_speed(1.5f);
		snake1[i].init();
		snake1[i].ignore_limit_walls();
	}

	// Se inicializa la segunda serpiente
	for (size_t i{ 0 }; i != snake2.size(); ++i)
	{
		snake2[i].get_transform().set_pos(vec3(3, 1 + (0.75f * i), 0));
		//scene.print(std::to_string(wall_arr[i].get_transform().get_position().x));
		snake2[i].get_transform().scale(vec3(0.25, 0.25, 0.0001));
		snake2[i].move_down();
		snake2[i].set_speed(1.5f);
		snake2[i].init();
		snake2[i].ignore_limit_walls();
	}

	// Se inicializa la tercera serpiente
	for (size_t i{ 0 }; i != snake3.size(); ++i)
	{
		snake3[i].get_transform().set_pos(vec3(-2.f, 1 + (i * 0.75f), 0));
		//scene.print(std::to_string(wall_arr[i].get_transform().get_position().x));
		snake3[i].get_transform().scale(vec3(0.25, 0.25, 0.0001));
		snake3[i].move_down();
		snake3[i].set_speed(1.2f);
		snake3[i].init();
		snake3[i].ignore_limit_walls();
	}

	// Se inicializa la cuarta serpiente
	for (size_t i{ 0 }; i != snake4.size(); ++i)
	{
		snake4[i].get_transform().set_pos(vec3(1, 1 + (0.75f * i), 0));
		//scene.print(std::to_string(wall_arr[i].get_transform().get_position().x));
		snake4[i].get_transform().scale(vec3(0.25, 0.25, 0.0001));
		snake4[i].move_down();
		snake4[i].set_speed(1.f);
		snake4[i].init();
		snake4[i].ignore_limit_walls();
	}
	snake2[0].get_gameobject().add_tag("check");
	// Triggers de cambio de direccion de la primera serpiente
	trigger_left1.get_transform().translate(vec3(-5, 3.9, 0));
	trigger_left1.get_transform().scale(vec3(1, 1, 0.0001));
	trigger_left1.get_gameobject().add_tag("Down Trigger");

	trigger_right1.get_transform().translate(vec3(5, -3.9, 0));
	trigger_right1.get_transform().scale(vec3(1, 1, 0.0001));
	trigger_right1.get_gameobject().add_tag("Up Trigger");

	trigger_up1.get_transform().translate(vec3(4.9, 4, 0));
	trigger_up1.get_transform().scale(vec3(1, 1, 0.0001));
	trigger_up1.get_gameobject().add_tag("Left Trigger");

	trigger_down1.get_transform().translate(vec3(-4.9, -4, 0));
	trigger_down1.get_transform().scale(vec3(1, 1, 0.0001));
	trigger_down1.get_gameobject().add_tag("Right Trigger");

	vec3 scale{ 0.05, 0.05, 0.00001 };

	// Triggers de cambio de direccion de la segunda serpiente
	trigger_left2.get_transform().translate(vec3(-3.2, 2.3, 0));
	trigger_left2.get_transform().scale(scale);
	trigger_left2.get_gameobject().add_tag("Right Trigger");

	trigger_right2.get_transform().translate(vec3(3.2, -2.3, 0));
	trigger_right2.get_transform().scale(scale);
	trigger_right2.get_gameobject().add_tag("Left Trigger");

	trigger_up2.get_transform().translate(vec3(3.3, 2.3, 0));
	trigger_up2.get_transform().scale(scale);
	trigger_up2.get_gameobject().add_tag("Down Trigger");

	trigger_down2.get_transform().translate(vec3(-3.3, -2.3, 0));
	trigger_down2.get_transform().scale(scale);
	trigger_down2.get_gameobject().add_tag("Up Trigger");


	// Triggers de cambio de direccion de la tercera serpiente
	trigger_left3.get_transform().translate(vec3(-2.4, 1.4, 0));
	trigger_left3.get_transform().scale(scale);
	trigger_left3.get_gameobject().add_tag("Down Trigger");

	trigger_right3.get_transform().translate(vec3(2.4, -1.5, 0));
	trigger_right3.get_transform().scale(scale);
	trigger_right3.get_gameobject().add_tag("Up Trigger");

	trigger_up3.get_transform().translate(vec3(2.3, 1.5, 0));
	trigger_up3.get_transform().scale(scale);
	trigger_up3.get_gameobject().add_tag("Left Trigger");

	trigger_down3.get_transform().translate(vec3(-2.3, -1.5, 0));
	trigger_down3.get_transform().scale(scale);
	trigger_down3.get_gameobject().add_tag("Right Trigger");


	// Triggers de cambio de direccion de la cuarta serpiente
	trigger_left4.get_transform().translate(vec3(-1.2, 0.8, 0));
	trigger_left4.get_transform().scale(scale);
	trigger_left4.get_gameobject().add_tag("Right Trigger");

	trigger_right4.get_transform().translate(vec3(1.2, -0.8, 0));
	trigger_right4.get_transform().scale(scale);
	trigger_right4.get_gameobject().add_tag("Left Trigger");

	trigger_up4.get_transform().translate(vec3(1.3, 0.8, 0));
	trigger_up4.get_transform().scale(scale);
	trigger_up4.get_gameobject().add_tag("Down Trigger");

	trigger_down4.get_transform().translate(vec3(-1.3, -0.8, 0));
	trigger_down4.get_transform().scale(scale);
	trigger_down4.get_gameobject().add_tag("Up Trigger");

}
