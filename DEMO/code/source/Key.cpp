#include "Key.hpp"

Key::Key(Scene& s, size_t num) : number{ num }, cube_component{ "../../../DEMO/assets/number" + std::to_string(number) + std::string(".obj") }, Mono_behaviour(s, std::string("Key") + std::to_string(number))
{
	game_object.add_component(collider);
	game_object.add_component(cube_component);
	collider.only_passive = true;

	game_object.add_tag("Key");
	transform.scale(Vector3(0.035f, 0.035f, 0.00001f));
}

void Key::take()
{
	transform.translate(vec3(200, 200, 0));
	if (door_ptr) door_ptr->open();
}

void Key::drop()
{
	transform = init_tranform;
	if (door_ptr) door_ptr->close();
}
