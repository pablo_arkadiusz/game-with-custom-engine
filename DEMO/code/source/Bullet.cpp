#include "Bullet.hpp"

Bullet::Bullet(Scene& s) : Mono_behaviour(s, "bullet")
{
	game_object.add_component(collider);
	game_object.add_component(graphic);

	collider.only_passive = true; // Evita la llamada de callbacks del trigger

	// Ajustes del transform de la bala
	transform.scale(Vector3(0.15, 0.15, 0.0001));
	transform.set_pos(Vector3(9, 9, 0));

	// Se pone el tag "Wall" que es el encargado de matar al jugador con el contacto
	game_object.add_tag("Wall");
}

void Bullet::shoot(const vec3 &pos, const vec2 &direction)
{
	run = true;
	this->direction.x = direction.x * speed;
	this->direction.y = direction.y * speed;

	transform.set_pos(pos);
}

void Bullet::change_direcion()
{
	direction.x *= -1; direction.y *= -1;
}

void Bullet::movement(float delta_time)
{
	if(run) transform.translate(glt::Vector3(direction.x * delta_time, direction.y * delta_time, 0.f));
}

void Bullet::update(float delta_time)
{
	movement(delta_time);
}
