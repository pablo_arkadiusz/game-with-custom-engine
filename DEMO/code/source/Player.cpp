
#include <Player.hpp>

Player::Player(Scene& s, Audio_manager& audio_manager, const std::string& name) : audio_manager{ audio_manager }, Mono_behaviour(s, name)
{
	add_components();

	transform.scale(Vector3(0.1, 0.1, 0.0001));

	game_object.add_tag("Player");

	bind_input_keys();
	respawn();
}

void Player::try_take_key(Key& key)
{
	if (keys.size() == key.get_number() - 1)
	{
		keys.push_back(&key);
		key.take();
		audio_manager.play_pickup();
	}
	else audio_manager.play_bad_pickup();
}

void Player::add_components()
{
	game_object.add_component(controller);
	game_object.add_component(cube);
	game_object.add_component(collider);
}

void Player::bind_input_keys()
{
	controller.bind_action("Move Left", &Player::go_left, this);
	controller.bind_action("Stop Moving Left", &Player::stop_left, this);
	controller.bind_action("Move Right", &Player::go_rigth, this);
	controller.bind_action("Stop Moving Right", &Player::stop_right, this);
	controller.bind_action("Move Up", &Player::go_up, this);
	controller.bind_action("Stop Moving Up", &Player::stop_up, this);
	controller.bind_action("Move Down", &Player::go_down, this);
	controller.bind_action("Stop Moving Down", &Player::stop_down, this);
}

void Player::drop_all_keys()
{
	for (Key* k : keys) k->drop();
	keys.clear();
}

void Player::respawn()
{
	transform.set_pos(spawn_point);
	drop_all_keys();
}

void Player::manage_movement(float delta_time)
{
	const float speed = delta_time * move_speed;
	if (move_up)
		get_transform().translate(glt::Vector3(0.f, speed, 0.f));
	if (move_down)
		get_transform().translate(glt::Vector3(0.f, -speed, 0.f));
	if (move_right)
		get_transform().translate(glt::Vector3(speed, 0.f, 0.f));
	if (move_left)
		get_transform().translate(glt::Vector3(-speed, 0.f, 0.f));
}

void Player::update(float delta_time)
{
	manage_movement(delta_time);
}

void Player::on_trigger_enter(Game_object& other)
{
	if (other.check_tags({ "Wall" }))
	{

		audio_manager.play_death();
		respawn();
	}

	else if (other.check_tags({ "Key" }))
		try_take_key(*dynamic_cast<Key*>(other.get_mono_ptr()));
}
