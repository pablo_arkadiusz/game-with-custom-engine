#include <Audio_manager.hpp>

Audio_manager::Audio_manager(Scene& scene) : Mono_behaviour(scene, "Audio Manager")
{
	Audio_player::get_instance();	// Se llama el constructor de la instancia del reproductor de musica

	game_object.add_component(audio_component);

	load_audio_files();

	// Se reproduce la musica del background
	if (background_music)
		audio_component.play_music(background_music);

	enabled = false; // Se desactivan las llamadas a update y start
}
void Audio_manager::load_audio_files()
{
	// Se cargan los audios
	death_sound = audio_component.load_audio("../../../DEMO/assets/life.wav");
	pickup_sound = audio_component.load_audio("../../../DEMO/assets/pickup.ogg");
	bad_pickup_sound = audio_component.load_audio("../../../DEMO/assets/bad_pickup.ogg");
	open_door_sound = audio_component.load_audio("../../../DEMO/assets/open-door.ogg");
	background_music = audio_component.load_music("../../../DEMO/assets/evilbest.ogg");
}

void Audio_manager::play_death()
{
	if (death_sound)
		audio_component.play_sound(death_sound);
}

void Audio_manager::play_pickup()
{
	if (pickup_sound)
		audio_component.play_sound(pickup_sound);
}

void Audio_manager::play_bad_pickup()
{
	if (bad_pickup_sound)
		audio_component.play_sound(bad_pickup_sound);
}

void Audio_manager::play_open_door()
{
	if (open_door_sound)
		audio_component.play_sound(open_door_sound);
}
