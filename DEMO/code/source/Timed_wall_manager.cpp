#include <Timed_wall_manager.hpp>
#include <Timed_wall.hpp>



void Timed_wall_managerL1::load_walls(std::array<Timed_wall, 4> &walls)
{
	for (size_t i{ 0 }; i != walls_arr.size(); ++i)
		walls_arr[i] = &walls[i];

	Vector3 s = Vector3(1.1, 0.6, 0.0001);

	for(Timed_wall * wall_ptr : walls_arr)
	{
		wall_ptr->change_y_direction();
		wall_ptr->get_transform().scale(s);
	}	
}

void Timed_wall_managerL1::init_walls(float delta_time)
{
	elapsed_time += delta_time;

	if (wall_index == 0)
	{
		++wall_index;
		walls_arr[0]->init();
	}	

	else if (wall_index == 1 && elapsed_time > init_time)
	{
		++wall_index;
		walls_arr[1]->init();
	}
	else if (wall_index == 2 && elapsed_time > init_time * 2)
	{
		++wall_index;
		walls_arr[2]->init();
	}

	else if (wall_index == 3 && elapsed_time > init_time * 3)
	{
		walls_arr[3]->init();
		enabled = false; // Desabilita la funcion update y start
	}
		
}

void Timed_wall_managerL1::update(float delta_time)
{
	// Su usa update para inicializar las paredes. 
	// Sin embargo una vez que se inicializaron todos el update se desactivara con enabled = false
	// Para no seguir consumiendo recursos
	init_walls(delta_time);
}
