#include "Enemy.hpp"

Enemy::Enemy(Scene& s) : Mono_behaviour(s, "Enemy")
{
	game_object.add_component(graphic);
	game_object.add_component(collider);
}
