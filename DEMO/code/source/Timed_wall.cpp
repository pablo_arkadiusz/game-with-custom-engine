#include "Timed_wall.hpp"

Timed_wall::Timed_wall(Scene& s) : Mono_behaviour(s, "Timed Wall")
{
	game_object.add_component(cube);
	game_object.add_component(collider);

	game_object.add_tag("Wall");
	game_object.add_tag("Timed Wall");
}

void Timed_wall::move(float delta_time)
{
	float move_speed{ delta_time * speed };
	transform.translate(vec3(direction.x * move_speed, direction.y * move_speed, 0));
}

void Timed_wall::cooldown(float delta_time)
{
	elapsed_stop_time += delta_time;
	if (elapsed_stop_time > stop_time)
	{
		elapsed_stop_time = 0;
		launch();
		change_y_direction();
	}
}

void Timed_wall::manage_movement(float delta_time)
{
	if (moving) move(delta_time);
	else cooldown(delta_time);
}

void Timed_wall::manage_rotation(float delta_time)
{
	if (rotation_speed) transform.rotate(vec3(0, 0, delta_time * rotation_speed));
}

void Timed_wall::start()
{
	launch();
}

void Timed_wall::update(float delta_time)
{
	if (initialized)
	{
		manage_movement(delta_time);
		manage_rotation(delta_time);		
	}
}

void Timed_wall::on_trigger_enter(Game_object& other)
{
	using namespace glt;
	if (other.check_tags({ "Edge Wall" }))
	{
		if (use_limit_walls)
			stop();
	}
	else if (other.check_tags({ "Left Trigger", "Right Trigger", "Up Trigger", "Down Trigger" }))
	{
		Mono_behaviour* other_mono{ other.get_mono_ptr() };
		if (!other_mono) return;
		Wall_trigger* trigger{ dynamic_cast<Wall_trigger*>(other_mono) };
		if (!trigger) return;
		const bool going_down{ direction.y < 0 }, going_right{ direction.x > 0 };

		// El resultado de la colision dependiendo del fps del momento va a dar un valor distinto.
		// Para solucionar esto se compensa calculando el margen de diferencia y aplicando una tranlacion
		if (other.check_tags({ "Left Trigger" }))
		{
			// Valor del margen que se ha salido respecto a la posicion de colision
			const float threshold
			{
				going_down ?
				trigger->get_collision_point().y - transform.get_position().y :
				transform.get_position().y - trigger->get_collision_point().y
			};

			// Se aplica la compensacion de la posicion
			if (threshold)
				transform.translate(going_down ? Vector3(-threshold, threshold, 0) : Vector3(-threshold, -threshold, 0));

			move_left(); // Se cambia de direccion
		}
		else if (other.check_tags({ "Down Trigger" }))
		{
			// Valor del margen que se ha salido respecto a la posicion de colision
			const float threshold
			{
				going_right ?
				transform.get_position().x - trigger->get_collision_point().x :
				trigger->get_collision_point().x - transform.get_position().x
			};

			// Se aplica la compensacion de la posicion
			if (threshold)
				transform.translate(going_right ? Vector3(-threshold, -threshold, 0) : Vector3(threshold, -threshold, 0));

			move_down(); // Se cambia de direccion
		}
		else if (other.check_tags({ "Right Trigger" }))
		{
			// Valor del margen que se ha salido respecto a la posicion de colision
			float threshold
			{
				!going_down ?
				transform.get_position().y - trigger->get_collision_point().y :
				trigger->get_collision_point().y - transform.get_position().y
			};

			// Se aplica la compensacion de la posicion
			if (threshold)
				transform.translate(!going_down ? Vector3(threshold, -threshold, 0) : Vector3(threshold, threshold, 0));

			move_right(); // Se cambia de direccion
		}
		else if (other.check_tags({ "Up Trigger" }))
		{
			// Valor del margen que se ha salido respecto a la posicion de colision
			float threshold
			{
				!going_right ?
				trigger->get_collision_point().x - transform.get_position().x :
				transform.get_position().x - trigger->get_collision_point().x
			};

			// Se aplica la compensacion de la posicion
			if (threshold)
				transform.translate(!going_right ? Vector3(threshold, threshold, 0) : Vector3(-threshold, threshold, 0));

			move_up(); // Se cambia de direccion
		}
	}
}
