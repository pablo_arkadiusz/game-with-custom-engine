#include "Enemy_shooter.hpp"

Enemy_shooter::Enemy_shooter(Scene& s, const size_t bullets, std::string name) : Mono_behaviour(s, name), bullet_amount{ bullets }
{
	create_bullets();

	game_object.add_component(graphic);

	// Ajuste del transform del shooter
	transform.scale(Vector3(0.05, 0.05, 0.00001));
}

void Enemy_shooter::create_bullets()
{
	for (size_t i{ 0 }; i != bullet_amount; ++i)
	{
		// La funcion que se encarga de eliminar la bala de la memoria
		auto deleter = [&pool = bullet_pool](Bullet* bullet) { pool.deallocate(bullet); };

		// Alojacion de la memoria para la bala
		Bullet* bullet_ptr{ bullet_pool.allocate(scene) };

		// Se anade la bala como un shared_ptr que usara el destructor creado
		bullet_ptr_list.push_back(std::shared_ptr<Bullet>(bullet_ptr, deleter));
	}
}

Enemy_shooter::bullet_ptr_pair Enemy_shooter::get_two_bullets()
{
	bullet_ptr_pair next_bullets{ std::make_pair(bullet_ptr_list[bullet_index], bullet_ptr_list[bullet_index + 1]) };
	bullet_index += 2;
	if (bullet_index == bullet_amount) bullet_index = 0;
	return next_bullets;
}

void Enemy_shooter::manage_bullet_directions(std::shared_ptr<Bullet> b1_ptr, std::shared_ptr<Bullet> b2_ptr)
{		
	// Si las balas van en la misma direccion se cambia para que vallan en direccion opuesta
	if (b1_ptr->get_speed() > 0 && b2_ptr->get_speed() > 0 || b1_ptr->get_speed() < 0 && b2_ptr->get_speed() < 0)
		b1_ptr->change_direcion();
}

void Enemy_shooter::shoot()
{
	bullet_ptr_pair next_bullets{ get_two_bullets() };

	// La direccion de disparo es la que esta mirando el shooter
	vec2 direction
	{
		cos(transform.get_rotation().z),
		sin(transform.get_rotation().z)
	};

	// Se dispara las balas y se hace que el disparo vaya en direcciones contrarias
	next_bullets.first->shoot(transform.get_position(), direction);
	next_bullets.second->shoot(transform.get_position(), direction);
	manage_bullet_directions(next_bullets.first, next_bullets.second);
}

void Enemy_shooter::try_shoot(float delta_time)
{
	if (elapsed_time_from_last_shoot > cooldown)
	{
		shoot();
		elapsed_time_from_last_shoot = 0;
	}
	elapsed_time_from_last_shoot += delta_time;
}

void Enemy_shooter::manage_rotation(float delta_time)
{
	transform.rotate(Vector3(0, 0, delta_time * rotation_speed));
}
