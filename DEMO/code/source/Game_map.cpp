#include "Game_map.hpp"

void Game_static_map::transform_walls()
{
	Transform& spawn_wall_transform{ spawn_wall1.get_transform() };
	spawn_wall_transform.scale(Vector3(0.4, 0.05, 0.0001));
	spawn_wall_transform.translate(Vector3(-5.4, 0.6, 0));

	Transform& spawn_wall_2_transform{ spawn_wall2.get_transform() };
	spawn_wall_2_transform.scale(Vector3(0.4, 0.05, 0.0001));
	spawn_wall_2_transform.translate(Vector3(-5.4, -0.6, 0));

	Transform& exit_wall1_transform{ exit_wall1.get_transform() };
	exit_wall1_transform.scale(Vector3(0.4, 0.05, 0.0001));
	exit_wall1_transform.translate(Vector3(5.4, -0.6, 0));

	Transform& exit_wall2_transform{ exit_wall2.get_transform() };
	exit_wall2_transform.scale(Vector3(0.4, 0.05, 0.0001));
	exit_wall2_transform.translate(Vector3(5.4, 0.6, 0));

	Transform& top_wall_transform{ top_limit.get_transform() };
	top_wall_transform.scale(Vector3(10, 0.5, 0.0001));
	top_wall_transform.translate(Vector3(0, 3.8, 0));

	Transform& bottom_wall_transform{ bottom_limit.get_transform() };
	bottom_wall_transform.scale(Vector3(10, 0.5, 0.0001));
	bottom_wall_transform.translate(Vector3(0, -3.8, 0));

	Transform& left_wall_transform{ left_limit.get_transform() };
	left_wall_transform.scale(Vector3(0.5, 5, 0.0001));
	left_wall_transform.translate(Vector3(-6.5, 0, 0));

	Transform& right_wall_transform{ right_limit.get_transform() };
	right_wall_transform.scale(Vector3(0.5, 5, 0.0001));
	right_wall_transform.translate(Vector3(6.5, 0, 0));

	top_limit.get_gameobject().add_tag("Edge Wall"); bottom_limit.get_gameobject().add_tag("Edge Wall");
	right_limit.get_gameobject().add_tag("Edge Wall"); left_limit.get_gameobject().add_tag("Edge Wall");
}
