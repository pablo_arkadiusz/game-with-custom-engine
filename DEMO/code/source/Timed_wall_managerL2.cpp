
#include <Timed_wall_managerL2.hpp>
#include <Timed_wall.hpp>



void Timed_wall_managerL2::load_walls(std::array<Timed_wall, 7> &timed_walls)
{
	// Se carga el array con las paredes
	for (size_t i{ 0 }; i != walls_arr.size(); ++i)
		walls_arr[i] = &timed_walls[i];

	// Se aplican los ajustes de las paredes
	for (Timed_wall* wall_ptr : walls_arr)
	{
		wall_ptr->get_transform().scale(Vector3(0.55, 0.05, 0.0001));
		wall_ptr->init();
		wall_ptr->set_cooldown(cooldown);
		wall_ptr->set_speed(speed);
		wall_ptr->set_rotation(rotation_speed);
	}

	// Se manejan las direcciones de las paredes
	walls_arr[0]->change_y_direction();
	walls_arr[3]->change_y_direction();
	walls_arr[4]->change_y_direction();
}



