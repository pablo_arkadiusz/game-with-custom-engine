/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Graphic_component.hpp>


namespace engine
{
	/// Componente que indica que un objeto es un cubo y se renderizara como tal
	/// @see Graphic_component
	/// @see Render_system
	class Cube_component : public Graphic_component
	{

	public: Cube_component() : Graphic_component("Cube") {}

	};
}