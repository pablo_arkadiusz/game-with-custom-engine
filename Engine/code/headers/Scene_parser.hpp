/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma region Includes

#include <string>
#include <memory>
#include <rapidxml.hpp>
#include <unordered_map>
#include <Object_pool.hpp>
#include <Light_component.hpp>
#include <Camera_component.hpp>
#include <Mesh_component.hpp>
#include <Cube_component.hpp>
#include <Sphere_component.hpp>
#include <Player_controller.hpp>
#include <Box_collider2d.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine
{
	class Scene;
	class Game_object;
	class Transform;
	class Component;
	class Window;
	class Mono_behaviour;
	class Light_component;
}

#pragma endregion


#pragma region clase Scene_parser

namespace engine
{
	/// Se encarga de parsear la informacion de un archivo xml y actualizar los datos de Gameobjects, Monobehaviours, Componentes, Transform... de la escena.
	class Scene_parser
	{
	public:
		Scene_parser() = default;
	public:
		/// Interpreta el contenido de un archivo XML con datos de una escena
		/// @param scene_file_path La ruta del archivo XML que se debe interpretar.
		/// @param s Referencia a la escena
		/// @param mapping Referencia al mapeado de los monobehaviours que ha creado el desarrollador del juego, necesario para actualizar sus datos segun el xml
		/// @return True si ha habido exito.False en caso contrario
		bool parse(const std::string& scene_file_path, Scene& s, std::unordered_map<std::string, Mono_behaviour*>& mapping);

	private:
		/// Parsea la informacion del xml y actualiza los datos de los objetos de la escena segun esto
		/// @param scene_node Nodo a la escena del xml
		/// @param scene Referencia a la escena
		/// @param mapping Referencia al mapeado de los monobehaviours que ha creado el desarrollador del juego, necesario para actualizar sus datos segun el xml
		/// @return True si ha habido exito. False en caso contrario
		bool parse_scene(rapidxml::xml_node< >* scene_node, Scene& scene, std::unordered_map<std::string, Mono_behaviour*>& mapping);

		/// Parsea la informacion del xml para crear gameobject nuevos
		/// @note Principalmente se encarga de actualizar el transform y / o crea componentes para un objeto
		/// @param entity_node Nodo a la seccion de gameobjects / entidades del xml
		/// @param entity Referencia al Game_object que se actualiza
		/// @return True si ha habido exito. False en caso contrario
		bool parse_entity(rapidxml::xml_node< >* entity_node, Game_object& entity);

		/// Parsea la informacion del xml para crear gameobject nuevos
		/// @note Principalmente se encarga de actualizar la posicion, rotacion y escala del componente transform
		/// @param transform_node Nodo a la seccion de transform de un gameobject del xml
		/// @param transform Referencia al transform que se actualiza
		/// @return True si ha habido exito. False en caso contrario
		bool parse_transform(rapidxml::xml_node< >* transform_node, Transform& transform);

		/// Parsea la informacion del xml para actualizar la informacion de Monobehaviours creados por el desarrollador del juego
		/// @note Principalmente se encarga de actualizar el transform de los monobehaviours
		/// @param mono_node Nodo a la seccion de Monobehavious del xml
		/// @param mono_behaviour Referencia al Mono_behaviour que se actualiza
		/// @return True si ha habido exito. False en caso contrario
		bool parse_mono_behaviour(rapidxml::xml_node< >* mono_node, Mono_behaviour& mono_behaviour);

		/// Parsea la informacion del xml para actualizar la informacion de los componentes de un Game_object
		/// @note Principalmente se encarga de crear nuevos componentes que se hayan indicado en el archivo xml
		/// @param component_node Nodo a la seccion de componentes del un gameobject del xml
		/// @param component Referencia al componente que se actualiza
		/// @return True si ha habido exito. False en caso contrario
		bool parse_component(rapidxml::xml_node< >* component_node, Game_object& component);

	};
}

#pragma endregion
