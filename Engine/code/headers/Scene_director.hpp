#pragma once

#include <queue>
#include <Scene.hpp>
#include <initializer_list>
namespace engine
{

	/// Clase singleton encargada de controlar si una escena deberia ejecutarse o no
	class Scene_director
	{
	private:
		bool state{ true };

	private:
		Scene_director() = default;
	public:
		~Scene_director() = default;

	public:
		static Scene_director& get_instance()
		{
			static Scene_director director;
			return director;
		}
		
	public:
		void set_state(const bool new_state)
		{
			state = new_state;
		}

		bool can_play() const { return state; }
	};

}