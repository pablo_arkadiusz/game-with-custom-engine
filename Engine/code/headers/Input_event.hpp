/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


/// Guarda la informacion sobre los eventos de input
struct Input_event
{
	enum Type
	{
		QUIT,
		KEY_DOWN,
		KEY_UP
	}
	type;

	union Data
	{
		struct { int key_code; } keyboard;
		struct
		{
			float x, y;
			int   buttons;
		}
		mouse;
	}
	data;
};





