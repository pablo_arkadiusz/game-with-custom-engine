/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <Game_object.hpp>
#include <Camera_component.hpp>

#pragma endregion

#pragma region Clase Camera_object

namespace engine
{
	class Scene;

	/// Objeto camara 
	class Camera_object : public Game_object
	{

	public:
		/// Constructor de la camara <br>
		/// A�ade el componente de camara al gameobject
		/// @param s Escena a la que la camara pertenece
		/// @param name Nombre del objeto camara
		Camera_object(Scene& s, const std::string& name = "Camera") : Game_object(s, name)
		{
			add_component(camera_component);
			transform.translate(glt::Vector3(0.f, 0.f, 5.f));
		}

	private:
		Camera_component camera_component; ///< Componente camara del objeto

	};
}

#pragma endregion


