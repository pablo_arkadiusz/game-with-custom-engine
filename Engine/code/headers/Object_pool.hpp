/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Diciembre 2020
 */

#pragma once


#include <Memory_pool.hpp>
#include <initializer_list>

/// Pool de objetos para alojar memoria de forma mas eficiente
template<typename T>
class Object_pool
{

public:
	using value_type = T;
	using value_ptr_type = value_type*;
	using size_type = size_t;

private:
	size_type currently_allocated{ 0 }; ///< Cantidad de elementos alojados
	Memory_pool mem_pool; ///< Memory Pool que utilizara para alojar la memoria

public:

	Object_pool(Object_pool&&) = delete;
	Object_pool(const Object_pool&) = default; // Shallow copy

	Object_pool(size_type pool_sz) : mem_pool(pool_sz, sizeof(T)) 
	{
		if (pool_sz) mem_pool.init(); // Si pool_sz es 0 entonces no hara nada.
	}

	~Object_pool()
	{
		//assert(currently_allocated == 0); // Se comprueba memory leak
	}

public:
	/// Aloja espacio para un objeto
	template <typename ...ARGUMENTS >
	value_ptr_type allocate(ARGUMENTS&& ...args)
	{
		void* alloc_mem = mem_pool.allocate();

		if (alloc_mem)
		{
			++currently_allocated;
			return new (alloc_mem) value_type(std::forward< ARGUMENTS >(args)...);
			//return reinterpret_cast<value_ptr_type>(alloc_mem);
		}

		return nullptr;
	}

	/// Desaloja de la memoria el objeto indicado
	void deallocate(value_ptr_type element) 
	{
		if (element)
		{
			element->~value_type();
			mem_pool.deallocate(element);
			--currently_allocated;
		}
	}
};



