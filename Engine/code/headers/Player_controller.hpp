/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <IObserver.hpp>
#include <string>
#include <Scene.hpp>
#include <Keyboard.hpp>
#include <memory>
#include <Game_object.hpp>
#include <functional>
#include <unordered_map>
#include <any>

namespace engine 
{ 
	struct Message; 
	class Mono_Behaviour;
}

namespace engine
{
	/// Controlador del personaje que recibe mensajes de los input de entrada
	class Player_controller : public Component, public IObserver
	{		
		using funcion_map = std::unordered_map<std::string, std::function<void()>>;

	private:
		funcion_map actions;

	public:		
		/// Asigna una funcion a una Accion de Input para que se ejecute inmediatamente al detectarla
		template<typename Object_Type>
		void bind_action(const std::string &action_name, void(Object_Type::* func)(), Object_Type* instance)
		{
			actions[action_name] = std::bind(func, instance);
		}

	public:
		struct
		{
			bool Q{ false }, W{ false }, E{ false }, R{ false }, T{ false }, Y{ false }, U{ false }, I{ false }, O{ false },
				P{ false },A{ false }, S{ false }, D{ false }, F{ false }, G{ false }, H{ false }, J{ false }, K{ false },
				L{ false }, Z{ false }, X{ false }, C{ false }, V{ false }, B{ false }, N{ false }, M{ false };
		} player_input{}; ///< Mantiene los datos de todas las teclas, para saber si estan pulsadas o no

	public:
		Player_controller() : Component("Player Controller") {}

	private:
		void start_observing_events() override;  ///< Suscripcion a los mensajes de input (tanto los eventos de input como las acciones)
		void update_input_info(Message &m); ///< Actualiza todas las teclas que se estan pulsando
	public:
		/// Recibe mensajes relacionados con el input del Message_dispatcher. <br>
		/// En funcion de este actualiza los datos del input (player_input) y llama los metodos bindeados a acciones (actions)
		/// @param message Mensaje con la informacion sobre el input o evento de accion
		void handle(Message& message) override; 
		void on_added_to_gameobject(Game_object& game_object) override; ///< Callback al ser anadido a un gameobject. Llama a notify_listening_input()

	};

}