/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <memory>
#include <set>
#include <vector>
#include <IObserver.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine 
{
	class Task; 
	class Scene;
	struct Message;
}

#pragma endregion

#pragma region Clase Kernel

namespace engine
{
	/// Clase encargada de manejar el bucle principal y de ejecutar todas las tareas y sistemas
	/// @see Task
	class Kernel : public IObserver
	{

	#pragma region Variables

	private:
		/// Object funciton para poder segregar las tareas por orden de prioridad
		struct Task_ptr_cmp
		{
			bool operator()(const Task* lhs, const Task* rhs) const;
		}; 

		std::set<Task*, Task_ptr_cmp> recurrent_task_ptr_set; ///< Lista con todas las tareas recurrentes pendientes
		std::vector<Task*> consumible_task_ptr_vec; ///< Lista con todas las tareas consumibles pendientes

		Scene& scene;

		bool paused{ false }, ended{ false }, forced_exit{ false }; // Estados del kernel
	
	#pragma endregion

	#pragma region Metodos
	public:
		Kernel(Scene& scene);
		~Kernel() = default;

	private:
		void handle(Message& message) override; ///< Se encarga de recibir mensajes
		void start_observing_events() override; ///< Suscripcion a la escucha de eventos de tipo exit
	public:
		bool is_exit_state() { return forced_exit; }
		void exit(); ///< Se ha forzado el cierre del kernel por algun evento (como por ejemplo darle a la pestana X
		void pause(); ///< Pone el kernel en estado de pausa
		void stop(); ///< Acaba el bucle del kernel para acabar el nivel
		void resume(); ///< Saca el kernel del estado pausa
		void add_task(Task* task_ptr); ///< Anade una tarea a la lista de tareas pendientes
		void init(); ///< Inicializa el kernel
		void run() const; ///< Ejecuta todo el kernel en bucle, hasta que se reciba algun evento de Salida / Exit

	#pragma endregion
	
	};
}

#pragma endregion