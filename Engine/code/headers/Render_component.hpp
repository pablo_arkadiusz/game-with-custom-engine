/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


#pragma region Includes

#include <Component.hpp>
#include <memory>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine { class Game_object; class Render_system; };

#pragma endregion

#pragma region Clase Render_component

namespace engine
{
	/// Componente que Indica que el objeto se va a renderizar <br>
	/// Es clase padre de Graphic_component, Camera_component y Light_component
	/// @note Es una clase abstracta / virtual pura
	class Render_component : public Component
	{

	public:
		Render_component(std::string component_name);
		virtual ~Render_component() = default;
	public:
		virtual std::shared_ptr<glt::Node> get_render_node() const = 0; ///< Accede al puntero al Node necesario para renderizar.

		void on_added_to_gameobject(Game_object& game_object) override; ///< Callback al ser anadido a un gameobject.
		void notify_render_system(); ///< Indica al kernel que se debe activar el sistema render

	};
}

#pragma endregion

