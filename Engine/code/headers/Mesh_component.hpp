/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Graphic_component.hpp>
#include <string>

namespace engine
{

	/// Los Meshes son la primitiva principal de gr�ficas
	/// @see Graphic_component
	/// @see Render_system
	class Mesh_component : public Graphic_component
	{
	public: Mesh_component(const std::string & path) : Graphic_component(path) {}

	};

}