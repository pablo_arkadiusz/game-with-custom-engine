/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Component.hpp>


namespace engine { class Audio_task; }

struct Mix_Chunk;
struct _Mix_Music;

namespace engine
{

	/// Componente que permite a un objeto manejar el audio
	/// @see Audio_task
	class Audio_source : public Component
	{
		using Mix_Music = _Mix_Music;
	private:
		/// Puntero a la tarea del audio.
		/// @note Se utiliza para anadir nuevos audios al sistema del audio de la escena (Audio_task)
		Audio_task* audio_task_ptr{ nullptr }; //Es puntero ya que se asigna despues de asignarle el gameobject

	public:
		Audio_source();

	public: 
		Mix_Chunk *load_audio(const std::string& s); ///< Carga un "audio chunk" de la ruta especificada
		Mix_Music* load_music(const std::string& path); ///< Carga un archivo de musica de la ruta especificada
		void play_sound(Mix_Chunk * chunk); ///< Anade un "audio chunk" a la lista de audios pendientes por reproducir del sistema de audio de la escena
		void play_music(Mix_Music* music);  ///< Asigna un archivo de musica como una tarea pendiente en el sistema de audio de la escena
		void on_added_to_gameobject(Game_object& game_object); ///< Callback al ser anadido a un gameobject. Asigna el puntero al Audio Task de la escena

	};

}



