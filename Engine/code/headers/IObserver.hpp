/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <string>
#include <Input_event.hpp>

namespace engine { struct Message; }


namespace engine
{
	/// Clase que se usa para el patron de Observer / Listener
	class IObserver
	{
	public:
		virtual void handle(Message& message) = 0; ///< Recibe un mensaje enviado por el Message_dispatcher
		virtual void start_observing_events() = 0;  ///< Se suscribe a los tipos de eventos/ mensajes que quiera recibie

		IObserver() = default;
		virtual ~IObserver() = default;
	};
}