/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <string>

struct SDL_Window;
typedef void* SDL_GLContext;

namespace engine
{
	/// Clase encargada de crear y manejar una ventana para el juego
	class Window
	{	

	public:
		SDL_Window *window_ptr{ nullptr };
		SDL_GLContext context_ptr;
		
	public:
		Window(std::string t, const int w, const int h, bool fullscreen);
		~Window();

	public:
		void set_title(const std::string& new_title);

	public:
		unsigned get_width() const; ///< Retorna el ancho actual de la ventana (el usuario puede cambiarlo).
		unsigned get_height() const; ///< Retorna el alto actual de la ventana (el usuario puede cambiarlo). 

		void enable_vsync();
		void disable_vsync();	

		void clear() const; ///< Borra el buffer de la pantalla usando OpenGL.
		void swap_buffers() const; ///< Intercambia el buffer visible con el buffer oculto.
	};
}