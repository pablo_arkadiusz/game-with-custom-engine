/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Game_object.hpp>
#include <Light_component.hpp>



namespace engine
{
	/// Objeto luz 
	class Light_object : public Game_object
	{

	public:
		/// Constructor de la luz <br>
		/// A�ade el componente de camara al gameobject
		/// @param s Escena a la que la camara pertenece
		/// @param name Nombre del objeto camara
		Light_object(Scene& s, std::string name = "Light") : Game_object(s, name)
		{
			add_component(light_component);
			transform.translate(Vector3(5.f, 5.f, 5.f));
			//transform.scale(glt::Vector3(0.f, 0.f, 0.f));
		}

	private:
		Light_component light_component; ///< Componente luz del objeto

	};

}