/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <memory>
#include <Render_component.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace glt
{
	class Node;
	class Model;
}

#pragma endregion


#pragma region Clase Graphic_component

namespace engine
{
	/// Componente grafico que se renderizara por el sistema render: Render_system <br>
	/// Es la clase padre de Cube_component, Sphere_component y Mesh_component
	/// @attention En futuro se pueden anadir mas clases hijas como Esphere_component, Model_component, etc
	/// @see Component
	/// @see Cube_component
	/// @see Render_system
	class Graphic_component : public Render_component
	{

	private:
		std::shared_ptr<glt::Model> model_ptr; ///< Puntero al nodo que se usa en el sistema render para imprimirlo
		static size_t graphic_id; ///< Id usado para que ningun componente grafico tenga el mismo nombre
	public:
		Graphic_component(const std::string &type);
		virtual ~Graphic_component() = default;

	public:
		std::shared_ptr<glt::Node> get_render_node() const override; ///< Devuelve el puntero al nodo con el drawable elejido (Cubo, esfera, modelo, etc)
	};
}

#pragma endregion


