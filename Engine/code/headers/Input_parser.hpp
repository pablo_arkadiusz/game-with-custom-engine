/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <string>
#include <memory>
#include <rapidxml.hpp>
#include <unordered_map>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine { class Input_task; }

#pragma endregion

#pragma region Clase Input_parser

namespace engine
{
	/// Se encarga de parsear la informacion de un archivo xml y actualizar los datos de acciones predeterminadas del input. Como por ejemplo Saltar, Moverse alante, etc.
	class Input_parser
	{

	public:
		// Interpreta el contenido de un archivo XML con datos con las acciones de input
		// @param scene_file_path La ruta del archivo XML que se debe interpretar.
		// @param input_task Referencia a la tarea de input de una escena, necesario para actualizar sus eventos de acciones input
		// @return True si ha habido exito. False en caso contrario */
		bool parse(const std::string& scene_file_path, Input_task& input_task);

	private:
		/// Parsea la informacion del xml y actualiza los datos del input task en funcion de esto
		/// @param scene_file_path La ruta del archivo XML que se debe interpretar.
		/// @param input_task Referencia a la tarea de input de una escena, necesario para actualizar sus eventos de acciones input
		/// @return True si ha habido exito.False en caso contrario
		bool parse_input(rapidxml::xml_node< >* scene_node, Input_task& input_task);

		/// Devuelve el valor de un keycode a partir de una cadena string <br>
		/// Se utiliza para parsear una cadena del xml a un dato (int) que pueda ser usado como dato de evento
		int get_keycode(const std::string& key);

	};
}

#pragma endregion
