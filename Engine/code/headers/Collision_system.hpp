/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <Task.hpp>
#include <vector>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine
{
	class Collider2d;
	class Transform; 
}

#pragma endregion


#pragma region Clase Collision_system

namespace engine
{
	/// Sistema que se encarga de manejar las colisiones de los objetos. <br>
	/// Los componentes de colision al ser creado se anaden a la lista de componentes que este sistema controla y anaden el sistema a la lista de los sistemas del kernel <br>
	/// Finalmente durante la llamada del metodo run de la clase kernel se llama este sistema y este comprueba las colisiones. En caso de que las encuentre hace llamar <br>
	/// Los metodos on_trigger_enter, on_trigger_exit y on_trigger_stay de la clase Game_object <br>
	/// @see Mono_behaviour::on_trigger_enter
	/// @see Mono_behaviour::on_trigger_exit
	/// @see Mono_behaviour::on_trigger_stay
	/// @see Kernel
	/// @see Box_collider_2d
	/// @see Task
	/// @note Sus metodos initialize y run son privados. Sin embargo Su clase padre(Task) tiene la clase Kernel como friend
	class Collision_system : public Task
	{

	#pragma region Variables

	private:
		std::vector<Collider2d*> collider_list; ///< Vector con todos los componentes de colision que van a ser computados cada frame

	#pragma endregion

	#pragma region Metodos

	public:
		Collision_system() : Task(100) { }

	private:
		void check_collisions(); ///< Comprueba todas las colisiones posibles de los componentes de collider_list;

		/// Comprueba si dos Colisionadores Box_collider_2d se estan tocando
		/// @param c1 Uno de los dos componentes implicados en la colision
		/// @param c2 El otro componente
		/// @return True si c1 y c2 colisionan
		bool is_colliding(Transform& c1, Transform& c2);
		//bool is_colliding(Circle_collider& c1, Circle_collider& c2);

	public:
		void add_collider2d(Collider2d* comp); ///< Anade un componente de colision 2D a la lista de componentes de colision

	private:
		/// @note Actualmente sin uso
		void initialize() override {} 
		void run(float delta_time) override; ///< Se encarga de manejar las colisiones

	#pragma endregion

	};

}

#pragma endregion

