/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Task.hpp>
#include <Message.hpp>
#include <vector>
#include <IObserver.hpp>


namespace engine
{
	/// Sistema que envia notificaciones / mensajes a todas las clases que esten subscritas mediante un patron de Observer
	/// @see IObserver
	class Message_dispatcher : public Task
	{
		using id_type = Message::id_type;
		
	private:
		/// Mapa con todos los observadores que reciben mensajes  <br>
		/// Estos estan organizados segun el id o tipo de mensaje que quieren recibir
		std::unordered_map < id_type, std::vector<IObserver*>> observers; 
		std::vector<Message> pending_messages; ///< Mensajes que estan pendientes de enviar. Estos se acumulan en cualquier momento y se envian durante el metodo run

	public:
		Message_dispatcher(): Task(5, true) {}
	public:
		void add(id_type id, IObserver* observer); ///< Suscribe a una clase nueva para que reciba notificaciones de un id o tipo determinado
		void send(Message& message); ///< Anade un mensaje a la lista de mensajes pendientes
		void run(float delta_time) override; ///< Envia todos los mensajes pendientes
		void initialize() override {}
	};

}