/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


#pragma region Includes

#include <set>
#include <iterator>
#include <Component.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine 
{ 
	class Game_object;
	class Transform;
}

#pragma endregion

#pragma region Clase Box_collider_2d

namespace engine
{
	/// Componente que indica que un componente con 2 dimensiones tiene colisiones
	/// @see Component
	/// @see Collision_system
	class Collider2d : public Component
	{

	private: friend class Collision_system; ///< Solo la clase Collision_system puede encargarse de manejar las cosisiones

#pragma region Variables
		// Se usa la estructura std::set para llamar std::set_difference al comparar las colisiones del frame anterior con el frame actual
	private: std::set<size_t> curr_colliders; ///< Objetos (guardados por id) con los que estaba colisionando en el ultimo frame
	public: bool only_passive{ false }; ///< Un colider pasivo recibe colisiones pero no las genera. Tampoco genera callbacks de trigger

#pragma endregion

#pragma region Metodos

	private:
		/// Se encarga de diferenciar las colisiones recientes, las que se mantienen y las que acaban. <br>
		/// Invoca los callbacks Game_object::on_trigger_enter(), Game_object::on_trigger_stay y Game_object::on_trigger_exit().
		/// @param new_colliders Lista de los nuevos objetos con los que esta colisionando
		void manage_collision(const std::set<size_t>& new_colliders);

		void notify_collision_system(); ///< Indica al kernel que se debe activar el sistema de colisiones

	private:
		/// Comprueba si el objeto esta colisionando con otro
		/// @note Es un metodo virtual puro
		/// @param other_transform Transform del otro objeto con el que se va a comprobar la colision
		/// @return True si esta colisionando, False en caso contrario
		virtual bool is_colliding(Transform& other_transform) = 0;

	public:
		Collider2d(const std::string  &name);
		virtual ~Collider2d() = default;
	public:
		void on_added_to_gameobject(Game_object& game_object) override; ///< Callback al ser anadido a un gameobject. Llama a notify_collision_system()

#pragma endregion

	};
}

#pragma endregion

