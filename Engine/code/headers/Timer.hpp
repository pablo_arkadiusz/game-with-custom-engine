/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <chrono>
#include <iostream>

/// Cronometraje de tiempo
class Timer
{

private:
	using clock = std::chrono::high_resolution_clock;
	using time_point = std::chrono::time_point<clock>;
	using micro_sec = std::chrono::microseconds;

private:
	time_point start_time{ clock::now() };

public:
	Timer() = default;
	~Timer() = default;

	double get_elapsed();

	void stop();

};
