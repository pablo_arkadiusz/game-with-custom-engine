/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <unordered_map>
#include <variant>
#include <Input_event.hpp>

namespace engine { class Collider2d; }
namespace engine
{
	/// Hola
	struct Message
	{
		using id_type = size_t;
		using value_type = std::variant<int, Input_event::Type, std::string>;
		using id_map = std::unordered_map<id_type, value_type>;
		using pair_type = id_map::value_type;
	private:
		id_type id;
	public:
		id_type get_id() const { return id; }
		id_map data;
		
		
		/// Se intenta leer un tipo de ID mapeado a un determinado tipo. <br>
		/// Ejemplo de uso:
		/// @code
		/// Input_event::Type* input_type{ message.read<Input_event::Type>(ID(Key Type)) };
		/// if (!input_type) return;
		/// if (*input_type == Input_event::Type::KEY_DOWN) { /*...*/ }
		/// else if(*input_type == Input_event::Type::KEY_UP) { /*...*/ }
		/// @endcode 
		/// @return El valor vinculado a ese ID. Si no existe nullptr
		template<typename T>
		T *read(id_type id)
		{
			auto iter = data.find(id);
			return (iter != data.cend()) ? &std::get<T>(iter->second) : nullptr;
		}

		void change_id(id_type new_id)
		{
			id = new_id;
		}

	public: 
		bool operator == (const Message& other) const
		{
			return id == other.id && data == other.data;
		}

		Message(id_type _id): id{ _id } {}
	};

	/// Hasher del mensaje para poder usar mensajes con std::unordered_map
	// Se utiliza el algoritmo de Horner ( Horner's rule ) con un polinomial con x = 31
	class Message_hasher
	{
	public:
		size_t operator()(const Message& message) const
		{ 
			// Funcion hash que utiliza el unordered_map "data" para sacar el valor hash de cada uno de sus componentes
			Message::id_map::hasher hasher = message.data.hash_function();
			
			size_t hash{ 17 }; // Inicializacion de hash a numero primo

			// Primero se crea el valor hash para el mapa
			for (const Message::pair_type& it : message.data) hash = 31 * hash + (it.first);

			// Se anade el valor hash del ID
			hash = 31 * hash + std::hash<size_t>()(message.get_id());
		
			return hash;
		}
	};

}