/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <Task.hpp>
#include <vector>
#include <Audio_player.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine { class Audio_source; }

struct _Mix_Music;
struct Mix_Chunk;

#pragma endregion

#pragma region Clase Audio_task

namespace engine
{
	/// Tarea de reproducion de audios. <br>
	/// Mantiene una lista con todos los audios pendientes por reproducir y se encarga de reproducirlos durante <br>
	/// @note Al ser una tarea consumible, solo se reproduce una vez el audio y desaparece de la lista
	/// @see Kernel
	/// @see Task
	class Audio_task : public Task
	{
		using Mix_Music = _Mix_Music;

	public: Audio_task();

	private:
		std::vector<Mix_Chunk*> pending_chunk_list; ///< Lista de todos los audio chunks que estan pendientes por reproducir
		Mix_Music* pending_music_ptr_to_play = nullptr; ///< Puntero a la pista de musica pendiente por reproducir
		Audio_player &audio_player{ Audio_player::get_instance() }; ///< Reproductor de musica

	public:
		void add_chunk(Mix_Chunk* mix); ///< Anade un audio chunk a la lista de audios pendientes por reproducir
		void add_music(Mix_Music* mus); ///< Anade una pista de musica al puntero de musica pendiente por reproducir
	private:
		void run(float delta_time) override; ///< Reproduce todos los audios acumulados en la cola de audios
		void initialize() override {};

	};
}

#pragma endregion

