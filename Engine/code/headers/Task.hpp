/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


namespace engine
{
	/// Clase que representa una tarea. <br> 
	/// Las tareas pueden ser consumibles o duraderas. Y tienen un nivel de prioridad segun el cual se iran ejecutando en el Kernel
	/// @note Es una clase abstracta / virtual pura que sera usada poliformicamente por distintos tipos de tareas y/o sistemas
	/// @see Kernel
	/// @see Collision_system
	/// @see Input_task
	/// @see Update_task
	/// @see Render_system
	class Task
	{
		friend class Kernel; ///< El kernel es la unica clase que puede acceder a los metodos privados run() e initialize() de las tareas

	private:
		size_t priority; ///< Prioridad de ejecucion de la tarea
		const bool consumible;

	public:
		explicit Task(size_t priority, const bool consumible = false);
		virtual ~Task() = default;
	private:
		bool is_consumible() const; ///< Comprueba si es una tarea consumible o no
		virtual void run(float delta_time) = 0; ///< Ejecuta la tarea 
		virtual void initialize() = 0; ///< Inicializa la tarea o sistema

	public:
		bool operator < (const Task& rhs) const; ///< Un numero de prioridad mayor significa que la tarea ira antes

	};

}