/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <Window.hpp>
#include <Graphic_component.hpp>
#include <Kernel.hpp>
#include <Audio_player.hpp>
#include <Update_task.hpp>
#include <Render_system.hpp>
#include <Collision_system.hpp>
#include <Input_task.hpp>
#include <Audio_task.hpp>
#include <Update_task.hpp>
#include <Message_dispatcher.hpp>
#include <memory>
#include <functional>

#pragma endregion


#pragma region Declaraciones adelantadas

namespace engine
{
	class Game_object;
	class Task;
	class Mono_behaviour;
}

#pragma endregion

#pragma region Clase Scene

namespace engine
{
	/// Escena del juego.
	/// Se encarga de tener todos los objetos, sistemas y tareas del nivel y ejecutar y parar el kernel
	/// @note Los niveles pueden compartir la misma pantalla
	class Scene
	{

#pragma region Declaraciones de tipos

		// Tipos de mapas que guardan los punteros a los Gameobjects
		using name_pair = std::pair<std::string, std::shared_ptr<Game_object>>;
		using name_list_type = std::unordered_multimap<std::string, std::shared_ptr<Game_object>>;
		using id_list_type = std::unordered_map<size_t, std::shared_ptr<Game_object>>;
		using iter_pair = std::pair<name_list_type::iterator, name_list_type::iterator>;

#pragma endregion

	#pragma region Variables

	private:
		id_list_type id_map; ///< Tabla hash con todos los objetos mapeado por su id
		name_list_type name_map; ///< Tabla hash con todos los objetos mapeado por su nombre 
		Window& window; ///< Referencia a la ventana del juego
		Kernel kernel{ *this }; ///< Kernel que ejecuta el nivel

		// Sistemas y tareas
	private:
		Message_dispatcher message_dispatcher; ///< Sistema de mensajeria
		Render_system render_system{ *this }; ///< Sistema de renderizado
		Collision_system collision_system; ///< Sistema de colisiones
		Input_task input_system{ *this }; ///< Puntero al sistema de input
		Audio_task audio_task; ///< Tarea encargada de reproducir el audio de la escena
		Update_task update_task{ Update_task(id_map) }; ///< Task encargada de llamar el metodo start y update de los gameobjects

	#pragma endregion

	#pragma region Metodos

	public: 
		Scene(Window& window);
		~Scene() = default;

		#pragma region Getters y setters

	public:
		Render_system& get_render_system(); ///< Accede a una referencia al sitema render
		Collision_system& get_collision_system(); ///< Accede a una referencia al sistema de colision
		Input_task& get_input_system(); ///< Accene a una referencia la sistema de input
		Window& get_window() const; ///< Accede a una referencia a la ventana
		Audio_task& get_audio_task(); ///< Accede a una referencia a la tarea de reproduccion de audio
		Message_dispatcher& get_dispatcher(); ///< Accede al sistema de mensjeria ( Message_dispatcher )
		#pragma endregion
	
		#pragma region Busqueda de objetos

	public:

		void add_game_object(std::shared_ptr<Game_object> game_object_ptr); ///< Se anade un objeto a la escena para que pueda ser buscado por su nombre o su id

		/// Encuentra un gameobject de al escena
		/// @param name Nombre del objeto que se busca
		/// @return El objeto con el nombre indicado o nullptr en caso de no encontrarlo
		/// @attention Si hay varios objetos con el mismo nombre solo encontrara uno de ellos
		std::shared_ptr<Game_object> find_gameobject(const std::string& name); 

		/// Encuentra todos los gameobjects de la escena con el nombre indicado
		/// @param name Nombre de los gameobjects que se busca
		/// @return Un vector con punteros a los objetos con el nombre indicado. En caso de no encontar ninguno un vector vacio
		std::vector<std::shared_ptr<Game_object>> find_gameobjects(const std::string& name);

		/// Encuentra un gameobject con un determinado id
		/// @note Hay un id unico para cada gameobject
		/// @param id Numero id del gameobject que se busca
		/// @return El objeto con el id indicado o nullptr en caso de no encontrarlo
		std::shared_ptr<Game_object> find_gameobject_by_id(size_t id);

		#pragma endregion

		#pragma region iniciadores de sistemas y tareas

	public:
		void init_update(); ///< Inicializa el sistema update ( Update_task ). 
		void init_render(); ///< Inicializa el sistema render ( Render_system ).
		void init_input(); ///< Inicializa el sistema de input ( Input_task )
		void init_collision(); ///< Inicializa el sistema de colision ( Collision_system )
		void init_audio(); ///< Inicializa el sistema o tarea de audio ( Audio_task )
		void init_dispatcher(); ///< Inicializa el sistema de mensajeria ( Message_dispatcher )
		void init_kernel(); ///< Inicializa el kernel
		#pragma endregion

		#pragma region Estados de la partida

	public:
		void load_scene_xml(const std::string& path, std::unordered_map<std::string, Mono_behaviour*> &mapping); ///< Se cargan los ajustes de la escena mediante un archivo xml
		void load_input_xml(const std::string& path); ///< Se cargan las acciones de input de un archivo xml
		void print(std::string s); ///< warning Metodo temporal
		void run(); ///< Ejecuta el kernel
		//void pause(); ///< Pausa el juego
		void stop(); ///< Para el juego

		#pragma endregion

	#pragma endregion

	};
}
#pragma endregion


