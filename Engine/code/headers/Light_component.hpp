/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <memory>
#include <Render_component.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace glt
{
	class Light;
	class Node;
}

#pragma endregion


#pragma region Clase Light_component

namespace engine
{
	/// Componente que indica que un objeto tiene la funcionalidad de luz
	class Light_component : public Render_component
	{

	private: std::shared_ptr<glt::Light> light_ptr; ///< Puntero a la luz para que se usa en el render
	public: 
		Light_component();
		~Light_component() {}
	public: std::shared_ptr<glt::Node> get_render_node() const override;  ///< Devuelve puntero al Node necesario para renderizar. En este caso el render de la luz

	};
}

#pragma endregion




