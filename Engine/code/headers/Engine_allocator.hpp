#pragma once


#include <Object_pool.hpp>
#include <Light_component.hpp>
#include <Camera_component.hpp>
#include <Game_object.hpp>
#include <Scene.hpp>
#include <Sphere_component.hpp>
#include <Cube_component.hpp>
#include <Box_collider2d.hpp>

namespace engine
{
	/// Clase encargada de alojar memoria para objetos del motor
	class Engine_allocator
	{

	private:
		// TODO: sacar los valores de pool size segun el numero de elementos en la escena / juego o con macros que pueda cambiar el desarrollador en funcion del juego
		Object_pool <Light_component> light_component_pool{ 1 };
		Object_pool <Camera_component> camera_component_pool{ 1 };
		Object_pool <Game_object> gameobject_pool{ 2 };
		Object_pool <Sphere_component> sphere_component_pool{ 0 };
		Object_pool <Cube_component> cube_component_pool{ 0 };
		Object_pool <Box_collider2d> box_collider2d_pool{ 0 };

	private:
		Engine_allocator() = default;
	public:
		~Engine_allocator() = default;

	public:
		static Engine_allocator& get_instance()
		{
			static Engine_allocator instance;
			return instance;
		}

	public:
		inline Light_component* alloc_light_component();
		inline void dealloc_light_component(Light_component* comp_ptr);
		inline Camera_component* alloc_camera_component();
		inline void dealloc_camera_component(Camera_component* comp_ptr);
		inline Cube_component* alloc_cube_component();
		inline void dealloc_cube_component(Cube_component* comp_ptr);
		inline void dealloc_camera_component(Cube_component* comp_ptr);
		inline Sphere_component* alloc_sphere_component();
		inline void dealloc_sphere_component(Sphere_component* comp_ptr);
		inline Game_object* alloc_gameobject(Scene& s, const std::string& name);
		inline void dealloc_game_object(Game_object *);
		inline void dealloc_sphere_component(Game_object* gm);
		inline Box_collider2d* alloc_box2d_component();
		inline void dealloc_box2d_component(Box_collider2d* comp_ptr);
	};
}


// Se implementan en el mismo archivo ya que al ser inline falla el LINKER
namespace engine
{
	Light_component* Engine_allocator::alloc_light_component()
	{
		return light_component_pool.allocate();
	}
	void Engine_allocator::dealloc_light_component(Light_component* comp_ptr)
	{
		light_component_pool.deallocate(comp_ptr);
	}
	Camera_component* Engine_allocator::alloc_camera_component()
	{
		return camera_component_pool.allocate();
	}
	void Engine_allocator::dealloc_camera_component(Camera_component* comp_ptr)
	{
		camera_component_pool.deallocate(comp_ptr);
	}
	Cube_component* Engine_allocator::alloc_cube_component()
	{
		return cube_component_pool.allocate();
	}
	void Engine_allocator::dealloc_cube_component(Cube_component* comp_ptr)
	{
		cube_component_pool.deallocate(comp_ptr);
	}
	void Engine_allocator::dealloc_camera_component(Cube_component* comp_ptr)
	{
		cube_component_pool.deallocate(comp_ptr);
	}
	Sphere_component* Engine_allocator::alloc_sphere_component()
	{
		return sphere_component_pool.allocate();
	}
	void Engine_allocator::dealloc_sphere_component(Sphere_component* comp_ptr)
	{
		sphere_component_pool.deallocate(comp_ptr);
	}

	Game_object* Engine_allocator::alloc_gameobject(Scene& s, const std::string& name)
	{
		std::function<void(Game_object*)> deleter = [](Game_object* g_ptr) { Engine_allocator::get_instance().dealloc_game_object(g_ptr); };
		return gameobject_pool.allocate(s, name, deleter);
	}

	void Engine_allocator::dealloc_game_object(Game_object* gameobject_ptr)
	{
		gameobject_pool.deallocate(gameobject_ptr);
	}

	void Engine_allocator::dealloc_sphere_component(Game_object* gm)
	{
		gameobject_pool.deallocate(gm);
	}
	Box_collider2d* Engine_allocator::alloc_box2d_component()
	{
		return box_collider2d_pool.allocate();
	}
	void Engine_allocator::dealloc_box2d_component(Box_collider2d* comp_ptr)
	{
		box_collider2d_pool.deallocate(comp_ptr);
	}
}
