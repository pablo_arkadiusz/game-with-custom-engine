/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <math.hpp>
#include <Component.hpp>
#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine { class Game_object; }
using namespace glm;
using namespace glt;

#pragma endregion

#pragma region Clase Transform

namespace engine
{
	/// Componente de transformacion de los objetos. <br>
	/// Este permite cambiar la rotacion, posicion y escala. <br>
	/// Todos los gameobjects del juego tienen un componente Transform por defecto <br>
	/// @see Game_object
	class Transform : public Component
	{

	#pragma region Variables

	private:
		vec3 position{ 0, 0, 0 }; ///< Posicion en cordenadas cartesianas
		vec3 rotation{ 0, 0, 0 }; ///< Rotacion en angulos por cada eje
		vec3 ratio{ 1, 1, 1 }; ///< Escala de cada eje

	private:
		Matrix44 t_matrix; ///< Matrix de translacion
		Matrix44 r_matrix; ///< Matrix de rotacion
		Matrix44 s_matrix; ///< Matrix de escala
		Matrix44 matrix; ///< Matrix de transformacion total (translacion, rotacion y escala)

	#pragma endregion

	#pragma region Metodos

		#pragma region Constructores y destructor

	public:
		Transform();
		Transform(const Transform & t) = default;
		~Transform() = default;

		#pragma endregion

		#pragma region Transformaciones

	public:
		void set_pos(const Vector3 &new_pos); ///< Pone el objeto en la posicion indicada
		void translate(const vec3 &movement); ///< Mueve el objeto por la escena segun el vector indicado
		void scale(const vec3 &s); ///< Escala el objeto en cada eje segun el ratio indicado.
		void rotate(const vec3 &rot); ///< Rota el objeto los angulos indicados. @attention Temporalmente el eje z esta puesto por radianes
		void update_matrix(); ///< Extrae la matrix final ( matrix ) de las matrices parciales ( t_matrix, r_matrix y s_matrix ).

		#pragma endregion

		#pragma region Getters y setters

		vec3 get_position() { return position; }
		vec3 get_rotation() { return rotation; }
		vec3 get_scale() { return ratio; }
		Matrix44 get_matrix() { return matrix; }
		void set_matrix(const Matrix44 &new_matrix) { matrix = new_matrix; }
		void set_rotation(const vec3 &new_rot) { rotation = new_rot; }

		#pragma endregion

	#pragma endregion

	};
}

#pragma endregion
