/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Diciembre 2020
 */

#pragma once

#include <cstddef>
#include <iostream>
#include <memory>
#include <vector>
#include <assert.h> 
#include <internal/macros.hpp>

#pragma region clase Memory_pool

/// Pool de memoria para alojar elementos de forma mas eficiente.
/// @note Esta se compone de varios pools, y crece y decrece en funcion de los elementos alojados
class Memory_pool
{

	#pragma region Definicion de tipos

private:
	using size_type = size_t;
	using const_size_type = const size_type;
	using byte_ptr_type = std::byte*;

	/// Clase que se usa para crear todos los pools de memoria que sean necesarios
	struct Pool
	{
		byte_ptr_type begin_ptr{ nullptr }; ///< Puntero que apunta al comienzo del pool
		byte_ptr_type free_ptr{ nullptr }; ///< Puntero al siguiente elemento del pool que este libre para alojar un objeto
		size_type size{ 0 }; ///< Numero de elementos alojados que tiene el pool. Se utiliza para saber si un pool esta vacio o no y asi poder eliminarlo en caso necesario
	};

	#pragma endregion

	#pragma region Declaracion de tipos

private:
	const_size_type capacity; ///< Numero de elementos que tendra cada bloque pool. Si se aloja mas de estos se crearan bloques de Pool adicionales
	const_size_type item_size;  ///< Numero de bytes que ocupa el objeto alojado
	size_type index{ 0 }; ///< Indice de pool que se esta usando actualmente

	/// Alineacion de bits de memoria deseada. Si se usa un sistema de 32 bits sera 4 y si son 64 bits sera 8
	#if defined(_WIN32)
	#define ALIGNMENT 4
	#elif defined(_WIN64)
	#define ALIGNMENT 8
	#endif
		
		const_size_type mem_block_size { item_size + sizeof(byte_ptr_type) },
						remainder{ mem_block_size % ALIGNMENT }, ///< Resto de lo que ocupa el "nodo" en la memoria para crear la alineacion deseada
						padding{ ALIGNMENT - remainder }, ///< Espacio libre que queda en un bloque de memoria para crear la alineacion deseada
						chunk_size{ mem_block_size + padding }, ///< Tamano en bytes de lo que ocupara cada bloque de memoria en el pool
						pool_size{ chunk_size * capacity }; ///< Tamano en bytes del pool

	std::vector<Pool> pool_list; ///< vector con todos los pools que se estan usando para alojar elementos

	#pragma endregion

	#pragma region Metodos

		#pragma region Constructores y destructor

public:
	Memory_pool(size_type capacity, size_type item_sz);	
	Memory_pool(const Memory_pool& other) = default; // Con shallow copy esta ok
	Memory_pool(Memory_pool&&) = delete;

	~Memory_pool();

		#pragma endregion

		#pragma region Alojacion y desalojo
private:
	void allocate_new_pool();

	/// Borra todos los bloques de memoria que se han reservado
	void free();

	/// Metodo usado cuando se llena el pool que se esta usando. <br>
	/// Este intenta buscar un espacio libre en los pools existentes. Si no lo encuentra crea uno nuevo
	void change_pool();

	/// Comprueba si un puntero pertenece a un pool
	/// @param ptr Puntero que se comprobara si pertenece al pool indicado
	/// @param pool_begin Puntero al comienzo del pool del que se desea comprobar si ptr forma parte
	/// @return True en caso de que ptr pertenezca al pool indicado por pool_begin
	inline bool belong_to_pool(void* ptr, byte_ptr_type pool_begin);

public:
	/// Inicializa por primera vez el pool
	void init();

	/// Metodo que aloja un bloque de memoria 
	/// @return Puntero al espacio de memoria que se ha alojado
	void* allocate();

	/// Desaloja un espacio de memoria 
	/// @param slice_ptr Puntero al espacio de memoria que se desaloja
	void deallocate(void* slice_ptr);

		#pragma endregion

	#pragma endregion
};

#pragma endregion