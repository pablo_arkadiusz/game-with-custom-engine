/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <unordered_map>
#include <Transform.hpp>
#include <string>
#include <memory>
#include <functional>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine
{
	class Component;
	class Scene;
	class Mono_behaviour;
}

#pragma endregion

#pragma region Game_object

namespace engine
{
	/// Clase que indica que una entidad es un objeto de la escena. <br>
	/// Este poseera el componente transform para ser ubicado en la escena y podra poseer componentes
	/// @see Transform
	/// @see Component
	/// @see Scene
	class Game_object
	{

	#pragma region Variables

	protected:
		std::string name; ///< Nombre del objeto (Se permite usar nombres repetidos, ya que para diferenciarlos cada gameobject tiene su propio id
		Scene& scene; ///< Escena a la que pertenece
		Transform transform; ///< Componente transform para poder ser ubicado en la escena a traves de la rotacion, posicion y escala

	private:
		const size_t id; ///< Identificador del objeto para poder buscarlo a pesar de que haya otros objetos con el mismo nombre
		/// Tabla hash con nombre del componente y el componente
		/// @warning Al ser unordered_map se asume que un gameobject solo tendra un componente del mismo tipo, ya que en caso contrario los componentes tendran el mismo nombre y solo se podra buscar uno de ellos
		std::unordered_map<std::string, std::shared_ptr<Component>> components;
		static std::size_t objects_amount; ///< Numero de gameobjects creados. Se usa para asignar el id
		std::unordered_map<std::string, bool> tags; ///< Etiquetas asignadas a un gameobject
		Mono_behaviour* monobehaviour_ptr{ nullptr }; ///< Puntero a monobehaviour (para acceder a funciones como update, start y triggers en caso de que algun gameobject lo necesite, si no sera null)

	#pragma endregion

	#pragma region Metodos

			#pragma region Setters y getters

	public:
		void add_tag(const std::string &tag); ///< Asigna la identificacion deseada al objeto
		bool check_tags(const std::initializer_list<std::string> &_tags) const; ///< Comprueba si el objeto tiene alguno de los tags indicados
		size_t get_instance_id() const; ///< Obtiene el identificador del objeto
		std::string get_name() const; ///< Obtiene el nombre del objeto
		Scene& get_scene() const; ///< Obtiene una referencia a la escena a la que pertenece el objeto
		Transform& get_transform(); ///< Obtiene una referencia al componente de transform del objeto
		void set_monobehaviour(Mono_behaviour& monobehaviour); ///< Asigna un monobehaviour al que el gameobject estara vinculado para poder llamar funciones como update, start y triggers.
		Mono_behaviour* get_mono_ptr();

		/// Asigna un un componente transform al objeto 
		/// @note El objeto pasara a tener la posicion, rotacion y escala de dicho transform
		void set_transform(Transform t) { transform = t; }
		

			#pragma endregion

			#pragma region Constructores / Destructor

	public:
		Game_object(Scene& scene, const std::string& _name, std::function<void(Game_object*)> deleter = [](Game_object*) {});

			#pragma endregion

			#pragma region Busqueda de componentes

	public:
		/// Se anade un componente al gameobject
		/// @note Se asume que no hay que preocuparse por destruir el objeto
		/// @param comp Referencia al componente que se anadira al gameobject
		void add_component(Component &comp); 

		/// Se anade un componente al gameobject
		/// @note El shared_ptr se encargara de destruir el objeto, por lo tanto quizas sea necesario antes indicarle su metodo de destruccion
		/// @param comp_ptr Puntero al componente que se anadira al gameobject
		void add_component(std::shared_ptr<Component> comp_ptr);

		 /// Comprueba si el objeto tiene determinado componente
		/// @param comp_name Nombre del componente que se va a buscar
		/// @return True si el gameobject tiene ese objeto
		bool has_component(const std::string& comp_name) const;

		/// Busca en el objeto determinado componente
	    /// @param comp_name Nombre del componente que se va a buscar
		/// @return Componente que se ha buscado. En caso de no tenerlo devuelve nullptr
		std::shared_ptr<Component> find_component(const std::string& comp_name);

			#pragma endregion
	
	#pragma endregion

	};
}

#pragma endregion

