/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Include

#include <Task.hpp>
#include <vector>
#include <memory>

#pragma endregion


#pragma region Declaraciones declaraciones

namespace glt { class Render_Node; }
namespace engine
{
	class Render_component;
	class Scene;
}

#pragma endregion

#pragma region Clase Render_system

namespace engine
{


	/// Sistema que se encarga de renderizar los objetos. <br>
	/// Los componentes de renderizado (Render_component y sus clases hijas) al ser creado se anaden a la lista de componentes que este sistema controla y anaden el sistema a la lista de los sistemas del kernel <br>
	/// Finalmente durante la llamada del metodo run de la clase kernel se llama este sistema y este realiza las funciones de renderizado. <br>
	/// @see Kernel
	/// @see Graphic_component
	/// @see Camera_component
	/// @see Light_component
	/// @note Sus metodos initialize y run son privados. Sin embargo Su clase padre(Task) tiene la clase Kernel como friend
	class Render_system : public Task
	{

	private:
		std::vector<Render_component*> components_to_render; ///< Lista de componentes a renderizar
		Scene& scene; ///< Referencia a la escena a la que pertenece
		std::unique_ptr<glt::Render_Node> render_node_ptr; ///< Puntero al nodo render

	public:
		Render_system(Scene& scene);
		~Render_system();

	public:
		void add(Render_component* comp);  ///< Anade un componente de renderizado a la lista de componentes de render

	private:
		void render(); ///< Realiza el render de todos los componentes acumulados

	private:
		void initialize() override; ///< Inicializa el sistema render anadiendo los componentes al node de renderizado
		void run(float delta_time) override; ///< Se encarga de llamar el metodo render().

	};
}

#pragma endregion

