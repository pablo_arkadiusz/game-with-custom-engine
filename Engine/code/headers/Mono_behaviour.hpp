/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Game_object.hpp>

namespace engine
{
	class Scene;
}

namespace engine
{
	/// Clase base de la que todo script creado por el desarrollador hereda. <br>
	/// Este hace que el script reciba callbacks defunciones como update(), start() y eventos de trigger
	/// @see Mono_behaviour::on_trigger_enter()
	/// @see Mono_behaviour::on_trigger_exit()
	/// @see Mono_behaviour::on_trigger_stay()
	class Mono_behaviour
	{

		friend class Update_task; ///< Necesita acceso a start() y update()
		friend class Collider2d; ///< Necesita acceso a on_trigger_enter(), on_trigger_stay() y on_trigger_exit()

	protected:
		Game_object game_object; ///< Game_object pricnipal que controla el monobehaviour
		Transform& transform; ///< Referencia al transform del game_object que tiene el monobehaviour
		Scene& scene; ///< Referencia a la escena del monobehaviour
		bool enabled{ true }; ///< Si esta a true los callbacks de start y update estaran habilitados

	public:
		Mono_behaviour(Scene &s, const std::string& name);
		virtual ~Mono_behaviour() = default;

	public:
		Transform& get_transform(); ///< Accede a una referencia la transform del gameobject principal
		Game_object& get_gameobject(); ///< Accede a una referencia del gameobject principal
		void set_enabled(bool state); ///< Establece si los callbacks a Mono_behaviour::start() y Mono_behaviour::update() estan habilitados o no

#pragma region Callbacks

	private:
		virtual void start() {} ///< Se llamara una vez al iniciar la partida. Es llamado por Update_task
		virtual void update(float delta_time) {} ///< Se llamara una vez cada frame desde que comienza la partida. Es llamado por Update_task

		/// Se llama si el objeto esta colisionando con otro
		/// @param other Objeto con el que colisiona
		/// @note el objeto necesita tener un componente de colision para recibir este callback
		virtual void on_trigger_stay(Game_object& other) {}

		/// Se llama una vez cuando el objeto inicia una colision
		/// @param other Objeto con el que colisiona
		/// @note el objeto necesita tener un componente de colision para recibir este callback
		virtual void on_trigger_enter(Game_object& other) {}

		/// Se llama si el objeto deja de colisionar con un objeto
		/// @param other Objeto con el que deja de colisionar
		/// @note el objeto necesita tener un componente de colision para recibir este callback
		virtual void on_trigger_exit(Game_object& other) {}

#pragma endregion

	};

}

