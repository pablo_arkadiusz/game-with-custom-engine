/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


#pragma region Bibliotecas

#include <string>

#pragma endregion

#pragma region Declaracion de tipos

struct _Mix_Music;
struct Mix_Chunk;

#pragma endregion

#pragma region Clase Audio_player

namespace engine
{


	/// Reproductor de musica <br>
	/// Este tiene dos modos de reproducir audios: Musica y sonido chunk. <br>
	/// La musica solo tiene un canal principal y los chunks estan destinado a reproducir sonidos cortos en distintos canales <br>
	/// @note Es una clase singleton
	/// @warning Aun no esta optimizado bla bla bla
	/// @return True en caso de victoria
	/// @see Audio_source
	/// @see Audio_task
	class Audio_player
	{

	#pragma region Declaracion de tipos

		using size_type = size_t;
		using const_size_type = const size_type;
		using music_type = _Mix_Music;
		using sound_type = Mix_Chunk;
		using const_string = const std::string;
		using music_ptr_type = music_type*;
		using sound_ptr_type = sound_type*;
		using self_pointer = Audio_player*;
		using self_reference = Audio_player&;

	#pragma endregion

	#pragma region Definicion de tipos

		//Mix_Chunk* sound = nullptr;

	public:
		static self_reference get_instance(); ///< Accede a la instancia singleton del Audio_player

	#pragma endregion

	#pragma region Constructores y destructor

	private:
		Audio_player();
	public:
		~Audio_player();

	#pragma endregion

	#pragma region Metodos

	public:
		void create_sound_channels(const int channel_amount); ///< Aloja canales de audio para audio chunks

	public:
		/// Reproduce musica preferentemente larga o de fondo
		/// @param music Puntero al audio que reproducira
		/// @param loops Numero de veces que se reproducira, si es -1 sera infinito
		void play_music(music_ptr_type music, const int loops);

		/// Reproduce musica preferentemente larga o de fondo con un efecto fade
		/// @param music Puntero al audio que reproducira
		/// @param loops Numero de veces que se reproducira, si es -1 sera infinito
		/// @param ms Milisegundos que durara el efecto fade in
		void fade_in_music(music_ptr_type music, const int ms, const int loops);
		void pause_music(); ///< Pausa la musica que se esta reproduciendo 
		void resume_music(); ///< Reanuda la musica

		/// Reproduce un sonido preferentemente corto en un canal de audio elegido.
		/// @param sound Puntero al sonido que va a reproducir
		/// @param channel Canal en el que se reproducira, si es -1 se reproducira en el primero libre que encuentre
		/// @param loop Numero de veces que se reproducira, si es -1 sera infinito
		void play_sound(sound_ptr_type sound, const int channel, const int loop = 0);
		void pause_channel(const int channel_id); ///< Pausa todo sonido que se este reproduciendo en el canal indicado
		void resume_channel(const int channel_id); ///< Reanuda el sonido del canal indicado
		void pause_all_channels(); ///< Pausa el sonido en todos los canales
		void resume_all_channels(); ///< Reanuda el sonido de todos los canales

		void stop_audio(); ///< Detiene Todos los sonidos, sean musica o audio chunks
		void stop_music(); ///< Detiene la musica
		void stop_channel(const int channel_id); ///< Detiene los audios del canal indicado
		void stop_all_channels(); ///< Detiene los audios de todos los canales

		/// Carga un archivo de musica de la ruta indicada
		/// @param path Ruta del archivo de musica
		/// @return Puntero al archivo de musica cargado. Si la carga falla nullptr
		music_ptr_type load_music(const_string path);

		///< Carga un archivo audio chunk de la ruta indicada
		/// @param path Ruta del archivo de musica
		/// @return Puntero al archivo de audio cargado. Si la carga falla nullptr
		sound_ptr_type load_sound(const_string path);
		void free_music(music_ptr_type music); ///< Libera la memoria alojada por la musica
		void free_sound(sound_ptr_type sound); ///< Libera la memoria alojada por los audio chunks

	#pragma endregion
	};
}
#pragma endregion




