/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Graphic_component.hpp>

namespace engine
{

	/// Componente que indica que un objeto es una esfera y renderizara como tal
	/// @see Graphic_component
	/// @see Render_system
	class Sphere_component : public Graphic_component
	{

		public: Sphere_component() : Graphic_component("Sphere") {}
	
	};

}