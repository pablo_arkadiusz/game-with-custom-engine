/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once


#pragma region Includes

#include <memory>
#include <Render_component.hpp>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace glt
{
	class Camera;
	class Node;
}

#pragma endregion

#pragma region Clase Camera_component

namespace engine
{
	/// Componente que indica que un objeto tiene la funcionalidad de una camara
	class Camera_component : public Render_component
	{

	private:
		std::shared_ptr<glt::Camera> camera_ptr; ///< Puntero a la camara para que se usa en el render
		std::shared_ptr<glt::Node> get_render_node() const override; ///< Devuelve puntero al Node necesario para renderizar. En este caso el render de la camara
	public:
		Camera_component();
		~Camera_component() {}
	};
}

#pragma endregion




