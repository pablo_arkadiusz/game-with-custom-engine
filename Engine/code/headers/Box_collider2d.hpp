/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <set>
#include <iterator>
#include <Collider2d.hpp>
#include <vector>

#pragma endregion


#pragma region Clase Box_collider_2d

namespace engine
{

	namespace collision
	{
		struct vec2
		{
			vec2(float _x, float _y) { x = _x; y = _y; }
			float x, y; 
		};

		/// Rectangulo que se utiliza para calcular las colisiones de los objetos rectangulares y/o cuadrados
		struct Rectangle
		{
			Rectangle(vec2 pos, float w, float h, float angle);
			vec2 center_pos;
			float width, height, angle;

			/// Calcula la posicion de los vertices del rectangulo
			/// @return Array con los 4 puntos de las posiciones de los vertices del rectangulo
			std::array<vec2, 4> get_corners();
		};

		struct polygon
		{
			std::vector<vec2> p;	///< Vertices del poligono
			vec2 pos;				///< Posicion o centro del poligono
		};

		/// Clase que maneja los computos de las colisiones de los objetos rectangulares
		/// @see Box_collider_2d
		struct Box_collision
		{
			/// Comprueba la colision de dos rectangulos comprobando los lados de cada uno de estos
			/// @note Se utiliza para comprobar colisiones entre rectangulos que no tienen rotacion. Es mucho mas eficiente que el algoritmo SAT
			bool simple_rectangle_collision(Transform& t1, Transform& t2);

			/// Comprueba la colision de dos rectangulos comprobando los poligos de estos mediante el El Teorema del eje de separación (SAT) <br>
			/// Obtiene los 8 poligonos / vertices de dos componentes transform y los usa para llamar el metodo Box_collision::ShapeOverlap_SAT()
			/// @note Se utiliza para comprobar colisiones entre rectangulos que tienen un angulo de rotacion
			bool polygon_collision(Transform& t1, Transform& t2);

			/// Comprueba la colision de dos rectangulos comprobando los poligonos de estos mediante el El Teorema del eje de separación (SAT)
			/// @note Se utiliza para comprobar colisiones entre rectangulos que tienen un angulo de rotacion
			bool ShapeOverlap_SAT(polygon& r1, polygon& r2);
		};
	}


	/// Componente que indica que el objeto es de forma rectangular y tiene colisiones
	/// @see Component
	///	@see Collider2d
	/// @see Collision_system
	/// @attention Temporalmente esta clase no funciona bien con objetos Circle_collider2d (solo funciona con otros Box_collider2d)
	class Box_collider2d : public Collider2d
	{

	public:
		Box_collider2d();
		~Box_collider2d() = default;
	
	private:
		/// Comprueba si el objeto esta colisionando con otro
		/// @param other Transform del otro objeto con el que se va a comprobar la colision
		/// @return True si esta colisionando, False en caso contrario
		bool is_colliding(Transform& other) override;

		collision::Box_collision box_collision; ///< Objeto encargado de realizar los calculos para las colisiones
	};

}

#pragma endregion

