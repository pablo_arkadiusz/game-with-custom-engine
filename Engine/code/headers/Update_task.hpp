/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <Task.hpp>
#include <unordered_map>
#include <memory>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace engine { class Game_object; }

#pragma endregion


#pragma region Clase Update_task

namespace engine
{	
	/// Tarea encargada de ejecutar los metodos start y update de todos los gameobject de la escena
	/// @see Task
	/// @see Game_object::start()
	/// @see Game_object::update()
	class Update_task : public Task
	{

		using ptr_list_type = std::unordered_map<size_t, std::shared_ptr<Game_object>>;
		using pair_type = ptr_list_type::value_type; // Pareja de id y puntero a gameobject

	private:
		/// Mapa con los punteros a todos los Game_object de la escena guardados por su numero id
		ptr_list_type& object_ptr_list; //Es una referencia directa de los objetos de la escena

	public: Update_task(ptr_list_type& game_objects_ptrs); 

	public:
		void initialize() override; ///< Llama el metodo start de todos los objetos de la escena
		void run(float delta_time) override; ///< LLama el metodo update de todos los objetos de la escena

	};
}

#pragma endregion


