/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#include <Task.hpp>
#include <Keyboard.hpp>
#include <Input_event.hpp>
#include <unordered_map>
#include <Message.hpp>


namespace engine 
{
	class Message_dispatcher; 
	class Scene;
}
namespace engine
{
	/// Sistema de lectura y notificacion del input
	class Input_task : public Task
	{
		using input_map_type = std::unordered_map<Message, std::string, Message_hasher>;
	private:		
		Message_dispatcher &dispatcher; ///< Referencia al sistema de mensajeria (Message_dispatcher)

		///< Mapa que guarda todas las acciones de input <br>
		///< Se usa para comprobar si un mensaje es una accion de input o no <br>
		///< El mapa se carga mediante un archivo xml con el metodo Input_task::load_from_xml
		input_map_type input_mapping; 

	public:
		Input_task(Scene& s);

	public:
		/// Lee el input y lo envia a los objetos con Player_component 
		/// @see Task::run()
		void run(float delta_time) override;

		void add_input_action(const Message& event_message, const std::string& name); /// Anade un evento de accion de input

		/// Carga las acciones de input de un archivo xml, y rellena el mapa input_mapping segun esta informacion
		void load_from_xml(const std::string& path);

	private:
		/// Comprueba si ha habido algun evento de input
		/// @return True si ha encontrado un evento
		bool pool_event(Message& event);

	
		void initialize() override {}
	};
}
