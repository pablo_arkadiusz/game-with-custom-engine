/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <set>
#include <iterator>
#include <Collider2d.hpp>

#pragma endregion

namespace engine { class Transform; }


#pragma region Clase Circle_collider2d

namespace engine
{
	/// Componente que indica que un componente con forma circular tiene colisiones
	/// @see Component
	/// @see Collider2d
	/// @see Collision_system
	/// @attention Temporalmente esta clase no funciona bien con objetos Box_collider2d (solo funciona con otros Circle_collider2d)
	class Circle_collider2d : public Collider2d
	{

	public:
		Circle_collider2d();
		~Circle_collider2d() = default;

	private:
		/// Comprueba si el objeto esta colisionando con otro
		/// @param t2 Transform del otro objeto con el que se va a comprobar la colision
		/// @return True si esta colisionando, False en caso contrario
		bool is_colliding(Transform& t2) override;

	};
}

#pragma endregion

