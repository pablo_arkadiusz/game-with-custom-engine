/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#pragma once

#pragma region Includes

#include <string>

#pragma endregion

#pragma region Declaraciones adelantadas

namespace glt { class Node; }
namespace engine { class Game_object; }

#pragma endregion

#pragma region Clase Component

namespace engine
{

	/// Es la clase padre de todos los componentes como Box_collider_2d, Render_component y Collison_component
	/// @see Box_collider_2d
	/// @see Render_component
	/// @see Collision_component
	class Component
	{
		// TODO: crear una forma de poder desactivar y activar componentes
	
	#pragma region Variables

	private:
		std::string name; ///< Nombre del componente. Usado para buscarlo en la lista de componentes de un objeto
		Game_object* game_object_ptr{ nullptr }; // Es tipo pointer para poder asignarlo mas tarde con set_game_object_ptr. 

	#pragma endregion

	#pragma region Metodos

	// Getters y setters
	public:
		Game_object& get_game_object() const; ///< Accede al puntero al gameobject del componente
		void set_game_object_ptr(Game_object* gameobject_ptr); ///< Asigna el puntero al gameobject del componente
		std::string get_name() const; ///< Devuelve el nombre del componente

	// Constructores y destructor
	public:
		Component(std::string component_name);
		virtual ~Component() = default;
	public:
		virtual void on_added_to_gameobject(Game_object& game_object); ///< Callback al ser anadido a un gameobject.

	#pragma endregion

	};
}

#pragma endregion

