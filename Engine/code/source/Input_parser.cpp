/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Input_parser.hpp>
#include <fstream>
#include <sstream>
#include <Message.hpp>
#include <Input_event.hpp>
#include <Keyboard.hpp>
#include <Input_task.hpp>
#include <internal/id.hpp>

namespace engine
{

	using namespace std;
	using namespace rapidxml;

	bool engine::Input_parser::parse(const std::string& input_file_path, Input_task &input_task)
	{
		// Se lee el contenido de todo el archivo en la cadena xml_file_contents:

		std::string xml_file_contents;

		std::ifstream reader(input_file_path, ifstream::binary);

		if (!reader) return false;

		reader.seekg(0, ifstream::end);

		size_t file_size = size_t(reader.tellg());

		if (file_size == 0) return false;

		reader.seekg(0, ifstream::beg);

		xml_file_contents.resize(file_size);

		reader.read(&xml_file_contents.front(), file_size);

		if (reader.fail() || reader.bad()) return false;

		// Se parsea el contenido del archivo XML:

		xml_document< > document;

		document.parse< 0 >(const_cast<char*>(xml_file_contents.c_str()));

		// Se obtiene el tag ra�z (que deber�a ser <scene>) y se llama a la funci�n que lo interpretar�:

		xml_node< >* animations_node = document.first_node("Input_mapping");

		if (!animations_node) return false;

		return parse_input(animations_node, input_task);
	}


	bool Input_parser::parse_input(rapidxml::xml_node<>* event_node, Input_task& input_task)
	{
		for (xml_node< >* child = event_node->first_node("Event_mapping"); child; child = child->next_sibling())
		{
			// Si no existe cualquiera de estos atributos fallaria la lectura asique se retorna false
			if (!child->first_attribute("name") || !child->first_attribute("key_code") || !child->first_attribute("action"))
				return false;

			Message input_action_event(ID(Input Event));

			// Se extrae el nombre del evento
			std::string event_name;			
			std::istringstream name_stream(child->first_attribute("name")->value());
			getline(name_stream, event_name);
			
			// Se extrae la accion del evento (pressed o released)
			std::string action;	
			std::istringstream action_stream(child->first_attribute("action")->value());
			getline(action_stream, action);
				
			// Se extrae el keycode del evento
			std::string key_code;
			std::istringstream key_stream(child->first_attribute("key_code")->value());
			getline(key_stream, key_code);

			// Se anaden todos estos datos recopilados arriba al evento y se anade al mapa de acciones de input
			input_action_event.data[ID(Key Code)] = get_keycode(key_code);
			if (action == "pressed") input_action_event.data[ID(Key Type)] = Input_event::KEY_DOWN;
			else if (action == "released") input_action_event.data[ID(Key Type)] = Input_event::KEY_UP;
			input_task.add_input_action(input_action_event, event_name);
		}
		return true;
	}

	int Input_parser::get_keycode(const std::string& key)
	{
		// TODO: hacer el resto de teclas
		if (key == "w") return Keyboard::KEY_W;
		if (key == "s") return Keyboard::KEY_S;
		if (key == "a") return Keyboard::KEY_A;
		if (key == "d") return Keyboard::KEY_D;
		if (key == "e") return Keyboard::KEY_E;
		if (key == "r") return Keyboard::KEY_R;
		if (key == "t") return Keyboard::KEY_T;
		if (key == "y") return Keyboard::KEY_Y;
		return Keyboard::KEY_UNKOWN;
	}
}