/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Render_component.hpp>
#include <Scene.hpp>
#include <SDL.h>
#include <Game_object.hpp>
#include <Scene.hpp>
#include <Render_system.hpp>

namespace engine
{

	Render_component::Render_component(std::string component_name) : Component(component_name) {}

	void Render_component::on_added_to_gameobject(Game_object& game_object)
	{
		Component::on_added_to_gameobject(game_object);
		notify_render_system();
	}

	// Se necesita anadir el componente al sistema render despues de que haya sido asignado el gameobject, 
	// ya que necesita acceder a la escena, que estara en el gameobject, para tener acceso al sistema render 
	void Render_component::notify_render_system()
	{
		Scene& scene{ get_game_object().get_scene() };
		scene.get_render_system().add(this);
		//std::call_once(scene.get_render_flag(), [&scene]() { scene.init_render(); });
	}

}
