/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Circle_collider2d.hpp>
#include <Game_object.hpp>
#include <Transform.hpp>
#include <SDL_log.h>


namespace engine
{
	Circle_collider2d::Circle_collider2d() :Collider2d("Circle collider") {}

	bool Circle_collider2d::is_colliding(Transform& t2)
	{
		// Se comprueba la colision entre dos circulos
		Transform& t1{ get_game_object().get_transform() };
		float x{ t1.get_position().x - t2.get_position().x };
		float y{ t2.get_position().y - t2.get_position().y };
		float radio{ t1.get_scale().x + t2.get_scale().x };

		return x*x + y*y <= (radio)*radio;
	}
}