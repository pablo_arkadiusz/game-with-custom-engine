/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include "Task.hpp"

namespace engine
{
	Task::Task(size_t priority, const bool consumible) : consumible{ consumible }, priority{ priority } {}
	
	bool Task::is_consumible() const
	{
		return consumible;
	}
	bool Task::operator<(const Task& rhs) const
	{
		return priority > rhs.priority;
	}
}