/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <SDL.h>
#include <cassert>
#include <Window.hpp>
#include <OpenGL.hpp>
#include <internal/initialize.hpp>


namespace engine
{

	Window::Window(std::string title, const int w, const int h, bool fullscreen)
	{
		if (initialize(SDL_INIT_VIDEO))
		{
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

			window_ptr = SDL_CreateWindow
			(
				title.c_str(),
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				w, h,
				SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
			);

			assert(window_ptr != nullptr);

			if (window_ptr)
			{
				context_ptr = SDL_GL_CreateContext(window_ptr);

				assert(context_ptr != nullptr);

				if (context_ptr && glt::initialize_opengl_extensions())
					if (fullscreen)
						SDL_SetWindowFullscreen(window_ptr, SDL_WINDOW_FULLSCREEN_DESKTOP);				
			}
		}

		enable_vsync();
	}

	Window::~Window()
	{
		if (context_ptr) SDL_GL_DeleteContext(context_ptr);
		if (window_ptr) SDL_DestroyWindow(window_ptr);
		SDL_Quit();
	}
	

	void Window::set_title(const std::string& new_title)
	{
		if (window_ptr)	SDL_SetWindowTitle(window_ptr, new_title.c_str());
	}


	void Window::enable_vsync()
	{
		if (context_ptr) SDL_GL_SetSwapInterval(1);
	}

	void Window::disable_vsync()
	{
		if (context_ptr) SDL_GL_SetSwapInterval(0);
	}

	void Window::clear() const
	{
		if (context_ptr) glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void Window::swap_buffers() const
	{
		if (context_ptr) SDL_GL_SwapWindow(window_ptr);
	}

	unsigned Window::get_width() const
	{
		int width = 0, height;

		if (window_ptr) SDL_GetWindowSize(window_ptr, &width, &height);

		return static_cast<unsigned>(width);
	}

	unsigned Window::get_height() const
	{
		int width, height = 0;

		if (window_ptr) SDL_GetWindowSize(window_ptr, &width, &height);

		return static_cast<unsigned>(height);
	}



}