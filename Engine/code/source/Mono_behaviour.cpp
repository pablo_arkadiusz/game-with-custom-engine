/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Mono_behaviour.hpp>
#include <Scene.hpp>


namespace engine
{

	Mono_behaviour::Mono_behaviour(Scene& s, const std::string& name) : game_object{ s, name }, transform{ game_object.get_transform() }, scene{ s }
	{
		game_object.set_monobehaviour(*this);
	}

	Transform& Mono_behaviour::get_transform()
	{
		return transform;
	}

	Game_object& Mono_behaviour::get_gameobject()
	{
		return game_object;
	}

	void Mono_behaviour::set_enabled(bool state)
	{
		enabled = state;
	}

}

