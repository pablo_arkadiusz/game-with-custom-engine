/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Memory_pool.hpp>

Memory_pool::Memory_pool(size_type capacity, size_type item_sz) : item_size{ item_sz }, capacity{ capacity }
{ }

Memory_pool::~Memory_pool()
{
	free();
}

void Memory_pool::allocate_new_pool()
{
	pool_list.push_back(std::move(Pool{ nullptr, 0 }));

	byte_ptr_type byte_ptr = pool_list[index].free_ptr = pool_list[index].begin_ptr = new std::byte[pool_size]; // Se crea el memory pool con los bytes necesarios

	// Se interpreta la memoria de cada byte como si fuera un nodo, y se asigna los punteros next_ptr de cada uno
	for (size_type i{ 1 }; i != capacity; ++i, byte_ptr += chunk_size)
	{
		//reinterpret_cast<Node*>(byte_ptr)->next_ptr = (byte_ptr + chunk_size);
		byte_ptr_type next_ptr{ byte_ptr + sizeof(item_size) }; // Se va hacia la posicion de memoria de los bits que se interpretan como si fueran el "next_ptr"
		*reinterpret_cast<byte_ptr_type*>(next_ptr) = byte_ptr + chunk_size; // Se interpreta esta memoria como un puntero a un puntero, y se asigna el "next_ptr"
	}

	byte_ptr_type next_ptr{ byte_ptr + sizeof(item_size) }; // Se va hacia la posicion de memoria de los bits que se interpretan como si fueran el "next_ptr"
	*reinterpret_cast<byte_ptr_type*>(next_ptr) = nullptr; // Se interpreta esta memoria como un puntero a un puntero, y se asigna el "next_ptr"
}

void Memory_pool::free()
{
	for (Pool& p : pool_list) delete[] p.begin_ptr;
}

void Memory_pool::change_pool()
{
	// Se busca espacio libre en los pools existentes.
	size_type i{ 0 };
	for (; i != pool_list.size() && !pool_list[i].free_ptr; ++i)
		; // Null statement

	if (i != pool_list.size()) index = i; // En caso de encontrarlo se selecciona como pool actual
	else // En caso contrario se crea un pool nuevo y se selecciona como actual
	{
		++index;
		allocate_new_pool();
	}
}

void* Memory_pool::allocate()
{
	if (!pool_list[index].free_ptr) change_pool(); // Si el pool que se esta usando esta lleno se busca otro pool.

	void* alloc_node_ptr{ pool_list[index].free_ptr }; // Puntero al espacio de memoria que se aloja

	// free_ptr apunta a lo que apunta el bloque de memoria que se interpreta como "next_ptr"
	pool_list[index].free_ptr = *reinterpret_cast<byte_ptr_type*>(pool_list[index].free_ptr + sizeof(item_size));

	// Se aumenta el contador del numero de elementos del pool en el que se ha alojado el nuevo elemento
	++pool_list[index].size;

	return alloc_node_ptr;
}

inline bool Memory_pool::belong_to_pool(void* ptr, byte_ptr_type pool_begin)
{
	return (ptr >= pool_begin && ptr < pool_begin + pool_size);
}

void Memory_pool::init()
{
	allocate_new_pool();
}

void Memory_pool::deallocate(void* slice_ptr)
{
	// Se avanza hacia el pool del que forma parte el elemento que se quiere desalojar
	size_type d_index{ 0 }; // Indice del pool del que se desaloja
	for (; d_index != pool_list.size() && !belong_to_pool(slice_ptr, pool_list[d_index].begin_ptr); ++d_index)
		; // Null statement

	// Se asume que ese pool existe. Por lo tanto el indice sera menor que el numero de pools
	//assert(d_index < pool_list.size()); 

	byte_ptr_type old_free_ptr{ pool_list[d_index].free_ptr };

	// free_ptr apuntara al espacio de memoria que ocupaba el elemento que se esta desalojando
	pool_list[d_index].free_ptr = reinterpret_cast<byte_ptr_type>(slice_ptr);

	// La memoria que se interpreta como el "next_ptr" de este "nodo" apunta a donde apuntaba el free_ptr antiguo (old_free_ptr)
	*reinterpret_cast<byte_ptr_type*>(pool_list[d_index].free_ptr + sizeof(item_size)) = old_free_ptr;

	// Se intenta eliminar todo el pool en caso de que este quede vacio tras el desalojo.
	if (--pool_list[d_index].size == 0)
	{
		// En caso de que el pool del desalojo sea el pool que se esta usando, este no se eliminara (para evitar que este continuamente 
		// creando y borrando un pool en caso de que este alojando y desalojando justo cuando se llena el pool anterior)
		if (d_index != index)
		{
			delete[] pool_list[d_index].begin_ptr;

			// Ya que el orden de los pools da igual se mueve el pool a eliminar al final del vector y se elimina en tiempo constante
			std::iter_swap(pool_list.begin() + d_index, pool_list.end() - 1);
			pool_list.pop_back();

			// En caso de que se haya swapeado justo con el index que se esta usando, ahora ese index ocupara el index del pool que se elimino
			if (index == pool_list.size() - 1) index = d_index;
		}
	}
}
