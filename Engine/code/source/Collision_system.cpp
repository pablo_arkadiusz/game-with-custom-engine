/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Collision_system.hpp>
#include <Collider2d.hpp>
#include <Transform.hpp>
#include <SDL.h>
#include <Game_object.hpp>
#include <limits>
#include <array>
#include <math.h> /* sin */
#include <vector>
#include <Message.hpp>

namespace engine
{

	void Collision_system::add_collider2d(Collider2d* comp)
	{
		//SDL_Log("adding box collider: ");
		//SDL_Log(comp->get_name().c_str());
		collider_list.push_back(comp);
	}

	void Collision_system::run(float delta_time)
	{
		check_collisions();
	}

	void Collision_system::check_collisions()
	{
		// Se recorre todos los colliders para calcular sus colisiones
		for (Collider2d* col : collider_list)
		{		
			if (col->only_passive) continue; // Un colider pasivo recibe colisiones pero no las genera

			// Lista de objetos con los que colisiona este frame, se usara para manejar los metodos trigger: enter, exit y stay
			std::set<std::size_t> new_colliders; 

			// Se recorre todos los coliders de nuevo, para comprobar la colision con cada uno de ellos
			for (Collider2d* col2 : collider_list) 
			{
				// No se comprueba la colision consigo mismo
				if (col == col2) continue;

				Game_object& other_obj{ col2->get_game_object() };

				// Si hay colision se anade el collider a la lista de colisionadores
				if (col->is_colliding(other_obj.get_transform()))
					new_colliders.insert(other_obj.get_instance_id());
			}
			// Se maneja las colisiones para llamar a metodos trigger: enter, exit y stay
			col->manage_collision(new_colliders);
		}
	}






}
























//////////////////////////////////////////////////////////////////////////

