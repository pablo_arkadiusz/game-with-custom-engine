/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Message_dispatcher.hpp>
#include <SDL.h>
#include <internal/id.hpp>

namespace engine
{
	void Message_dispatcher::add(id_type id, IObserver* observer)
	{
		
		observers[id].push_back(observer);
	}

	void Message_dispatcher::send(Message& message)
	{
		pending_messages.push_back(message);
	}

	void Message_dispatcher::run(float delta_time)
	{
		using iter = std::unordered_map<id_type, std::vector<IObserver*>>::iterator;

		std::vector<Message> messages = std::move(pending_messages);
		
		for (Message& m : messages) // Se recorre toda la lista de mensajes acumulados
		{
			iter it{ observers.find(m.get_id()) };
			if (it != observers.cend())
				for (IObserver*& mes_observers : it->second) // Se recorre todos los observadores de mensajes con este ID
					mes_observers->handle(m); // Se envia el mensaje solo a estos observadores		
		}
	}

}
