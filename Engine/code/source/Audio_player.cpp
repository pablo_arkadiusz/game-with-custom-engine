/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */


#include <SDL.h>
#include <SDL_mixer.h>
#include <Audio_player.hpp>

namespace engine
{

	Audio_player::Audio_player()
	{
		SDL_Init(SDL_INIT_AUDIO);
		Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);

		Mix_Volume(-1, 10);
		Mix_VolumeMusic(10);
	}

	Audio_player::~Audio_player() 
	{
		Mix_CloseAudio(); 
	}


	// allocate mixing channels
	void Audio_player::create_sound_channels(const int channels_amount)
	{
		Mix_AllocateChannels(channels_amount);
	}

	void Audio_player::play_music(music_ptr_type music, const int loops = -1)
	{
		Mix_PlayMusic(music, loops);
	}

	void Audio_player::fade_in_music(music_ptr_type music, const int ms, const int loops = -1)
	{
		Mix_FadeInMusic(music, loops, ms);
	}

	void Audio_player::pause_music()
	{
		Mix_PauseMusic();
	}

	void Audio_player::resume_music()
	{
		Mix_ResumeMusic();
	}

	void Audio_player::play_sound(sound_ptr_type sound, const int channel, const int loop)
	{
		Mix_PlayChannel(channel, sound, loop);
	}

	void Audio_player::pause_channel(const int channel_id)
	{
		Mix_Pause(channel_id);
	}

	void Audio_player::resume_channel(const int channel_id)
	{
		Mix_Resume(channel_id);
	}

	void Audio_player::pause_all_channels()
	{
		pause_channel(-1);
	}

	void Audio_player::resume_all_channels()
	{
		resume_channel(-1);
	}

	void Audio_player::stop_audio()
	{
		stop_music();
		stop_all_channels();
	}

	void Audio_player::stop_music()
	{
		Mix_HaltMusic();
	}

	void Audio_player::stop_channel(const int channel_id)
	{
		Mix_HaltChannel(channel_id);
	}

	void Audio_player::stop_all_channels()
	{
		stop_channel(-1);
	}

	Audio_player::music_ptr_type Audio_player::load_music(const_string path)
	{
		return Mix_LoadMUS(path.c_str());
	}

	Audio_player::sound_ptr_type Audio_player::load_sound(const_string path)
	{
		return Mix_LoadWAV(path.c_str());;
	}

	void Audio_player::free_music(music_ptr_type music)
	{
		SDL_assert(music);
		Mix_FreeMusic(music);
	}

	void Audio_player::free_sound(sound_ptr_type sound)
	{
		SDL_assert(sound);
		Mix_FreeChunk(sound);
	}

	Audio_player& Audio_player::get_instance()
	{
		static Audio_player audio_player;
		return audio_player;
	}
}