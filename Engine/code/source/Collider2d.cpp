/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Collider2d.hpp>
#include <SDL.h>
#include <algorithm>
#include <Scene.hpp>
#include <Game_object.hpp>
#include <Mono_behaviour.hpp>
#include <string>

namespace engine
{
	Collider2d::Collider2d(const std::string &name) : Component(name) {}

	void Collider2d::on_added_to_gameobject(Game_object& game_object)
	{
		Component::on_added_to_gameobject(game_object);
		notify_collision_system(); // Se anade el collider al sistema de colisones
	}

	
	void Collider2d::manage_collision(const std::set<size_t>& new_colliders)
	{
		using namespace std;
		set<size_t> aux_set; ///< Set auxiliar para manejar las colisiones nuevas, las que se mantienen y las que acaban

		Game_object& obj{ get_game_object() };

		// aux_set se llena de los objetos que son diferentes respecto al frame anterior y al frame actual (los que antes estaban pero ahora no)
		// Y llama el metodo on_trigger_exit en cada uno de los monobehaviours
		set_difference(curr_colliders.begin(), curr_colliders.end(), new_colliders.begin(), new_colliders.end(), std::inserter(aux_set, aux_set.begin()));
		for (size_t obj_id : aux_set)
		{
			shared_ptr<Game_object> obj_ptr{ obj.get_scene().find_gameobject_by_id(obj_id) }; // Se accede al game object a partir de su id
			if (obj_ptr)
			{
				Mono_behaviour* mono{ obj.get_mono_ptr() }; // Se accede al monobehaviour del gameobject
				if(mono) mono->on_trigger_exit(*obj_ptr);
			}
			else SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Couldn't find collision's object id to process the collisions");
		}

		aux_set.clear();

		// aux_set se llena de los objetos que son diferentes respecto al frame anterior y al frame actual (los que antes no estaban pero ahora si)
		// Y llama el metodo on_trigger_enter en cada uno de los monobehaviours
		set_difference(new_colliders.begin(), new_colliders.end(), curr_colliders.begin(), curr_colliders.end(), std::inserter(aux_set, aux_set.begin()));

		for (size_t obj_id : aux_set)
		{
			shared_ptr<Game_object> obj_ptr{ obj.get_scene().find_gameobject_by_id(obj_id) }; // Se accede al game object a partir de su id	
			if (obj_ptr)
			{
				Mono_behaviour* mono{ obj.get_mono_ptr() }; // Se accede al monobehaviour del gameobject
				if(mono) mono->on_trigger_enter(*obj_ptr);
			}
			else SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Couldn't find collision's object id to process the collisions");
		}
		aux_set.clear();

		/// Se llama el metodo on_trigger stay de todos los objetos que tiene una colision
		for (size_t obj_id : new_colliders)
		{
			shared_ptr<Game_object> obj_ptr{ obj.get_scene().find_gameobject_by_id(obj_id) }; // Se accede al game object a partir de su id	
			if (obj_ptr)
			{
				Mono_behaviour* mono{ obj.get_mono_ptr() }; // Se accede al monobehaviour del gameobject
				if(mono) mono->on_trigger_stay(*obj_ptr);
			}
			else SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Couldn't find collision's object id to process the collisions");
		}

		curr_colliders = new_colliders;
	}


	void Collider2d::notify_collision_system()
	{
		// Se anade el collider al sistema de colisones
		Scene& scene{ get_game_object().get_scene() };
		scene.get_collision_system().add_collider2d(this);

		//std::call_once(scene.get_collision_flag(), [&scene]() { scene.init_collision(); });
	}

}