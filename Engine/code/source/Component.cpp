/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Component.hpp>
#include <SDL.h>
#include <Game_object.hpp>

namespace engine
{
	Game_object& Component::get_game_object() const
	{
		return *game_object_ptr; 
	}

	void Component::set_game_object_ptr(Game_object *gameobject_ptr)
	{
		game_object_ptr = gameobject_ptr;
	}

	std::string Component::get_name() const
	{
		return name; 
	}

	void engine::Component::on_added_to_gameobject(Game_object& game_object)
	{
		set_game_object_ptr(&game_object);
	}

	Component::Component(std::string component_name) : name{ component_name } {}

}