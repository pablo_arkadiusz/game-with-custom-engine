/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Transform.hpp>
#include <Game_object.hpp>
#include <SDL.h>

engine::Transform::Transform() : Component("Transform") {}

void engine::Transform::set_pos(const Vector3 &new_pos)
{
	translate(Vector3(-position.x, -position.y, -position.z));
	translate(new_pos);
}

void engine::Transform::translate(const vec3 &movement)
{
	t_matrix = glt::translate(t_matrix, movement);
	update_matrix();
}

void engine::Transform::rotate(const vec3 &rot)
{
	r_matrix = rotate_around_x(r_matrix, rot.x * 0.01745329251994329576923690768489f);
	r_matrix = rotate_around_y(r_matrix, rot.y * 0.01745329251994329576923690768489f);
	r_matrix = rotate_around_z(r_matrix, rot.z);
	update_matrix();
	rotation += rot;
}

void engine::Transform::update_matrix()
{
	matrix = t_matrix * r_matrix * s_matrix;
	position = extract_translation(matrix);
}

void engine::Transform::scale(const vec3 &s)
{
	s_matrix = glt::scale(s_matrix, s.x, s.y, s.z);
	ratio = s;
	update_matrix();
}
