/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Audio_task.hpp>
#include <SDL_mixer.h>


namespace engine
{
	Audio_task::Audio_task() : Task(1, true) {}

	void Audio_task::add_chunk(Mix_Chunk* mix)
	{
		pending_chunk_list.push_back(mix);
	}

	void Audio_task::add_music(Mix_Music* mus)
	{
		pending_music_ptr_to_play = mus;
	}

	void Audio_task::run(float delta_time)
	{
		std::vector<Mix_Chunk*>::iterator i = pending_chunk_list.begin();

		// Se reproducen todos los "audio chunks" pendientes
		while (i != pending_chunk_list.cend())
		{
			audio_player.play_sound(*i, -1);
			i = pending_chunk_list.erase(i);	
		}

		// Se reproduce la musica pendiente
		if (pending_music_ptr_to_play)
		{
			audio_player.play_music(pending_music_ptr_to_play, -1);
			pending_music_ptr_to_play = nullptr;
		}
	}
}