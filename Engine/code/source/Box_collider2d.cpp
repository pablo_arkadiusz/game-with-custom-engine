/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Box_collider2d.hpp>
#include <array>
#include <glm/glm.hpp>
#include <vector>
#include <Game_object.hpp>
#include <Transform.hpp>
#include <SDL.h>

namespace engine
{	
	Box_collider2d::Box_collider2d() : Collider2d("Box Collider2d") {}

	using namespace glm;

	bool Box_collider2d::is_colliding(Transform& t2)
	{
		Transform& t1{ get_game_object().get_transform() };
		// Si los rectangulos no tienen rotacion se realiza la comprobacion simple.
		// En caso de tener rotacion se comprueba con el algoritmo SAT
		return (t1.get_rotation().z == 0 && t2.get_rotation().z == 0) ? 
			box_collision.simple_rectangle_collision(t1, t2) : box_collision.polygon_collision(t1, t2);
	}

	namespace collision
	{
		// codigo fuente del algoritmo: https://github.com/OneLoneCoder/olcPixelGameEngine/blob/master/Videos/OneLoneCoder_PGE_PolygonCollisions1.cpp
		bool Box_collision::ShapeOverlap_SAT(polygon& r1, polygon& r2)
		{
			polygon* poly1 = &r1;
			polygon* poly2 = &r2;

			for (int shape = 0; shape < 2; shape++)
			{
				if (shape == 1)
				{
					poly1 = &r2;
					poly2 = &r1;
				}

				for (int a = 0; a < poly1->p.size(); a++)
				{
					int b = (a + 1) % poly1->p.size();
					vec2 axisProj = { -(poly1->p[b].y - poly1->p[a].y), poly1->p[b].x - poly1->p[a].x };
					float d = sqrtf(axisProj.x * axisProj.x + axisProj.y * axisProj.y);
					axisProj = { axisProj.x / d, axisProj.y / d };

					// Work out min and max 1D points for r1
					float min_r1 = INFINITY, max_r1 = -INFINITY;
					for (int p = 0; p < poly1->p.size(); p++)
					{
						float q = (poly1->p[p].x * axisProj.x + poly1->p[p].y * axisProj.y);
						min_r1 = std::min(min_r1, q);
						max_r1 = std::max(max_r1, q);
					}

					// Work out min and max 1D points for r2
					float min_r2 = INFINITY, max_r2 = -INFINITY;
					for (int p = 0; p < poly2->p.size(); p++)
					{
						float q = (poly2->p[p].x * axisProj.x + poly2->p[p].y * axisProj.y);
						min_r2 = std::min(min_r2, q);
						max_r2 = std::max(max_r2, q);
					}

					if (!(max_r2 >= min_r1 && max_r1 >= min_r2))
						return false;
				}
			}
			return true;
		}

		bool Box_collision::simple_rectangle_collision(Transform& t1, Transform& t2)
		{
			Vector3 pos1{ t1.get_position() }, pos2{ t2.get_position() };
			
			// Se obtiene el ancho y largo de los dos rectangulos
			const float width1{ t1.get_scale().x }, width2{ t2.get_scale().x };
			const float height1{ t1.get_scale().y }, height2{ t2.get_scale().y };

			// Se obtiene cada uno de los lados de los rectangulos
			const float r1_right_edge{ pos1.x + width1 }, r1_left_edge{ pos1.x - width1 };
			const float r1_down_edge{ pos1.y - height1 }, r1_up_edge{ pos1.y + height1 };
			const float r2_right_edge{ pos2.x + width2 }, r2_left_edge{ pos2.x - width2 };
			const float r2_down_edge{ pos2.y - height2 }, r2_up_edge{ pos2.y + height2 };

			// Se comprueba si hay colision comprobando las posiciones de los lados
			return (r1_right_edge >= r2_left_edge && r1_left_edge <= r2_right_edge &&
				    r1_up_edge >= r2_down_edge && r1_down_edge <= r2_up_edge);
			
		}

		bool Box_collision::polygon_collision(Transform& t1, Transform& t2)
		{
			// Se obtiene el ancho y largo de los dos rectangulos
			const float width1{ t1.get_scale().x }, width2{ t2.get_scale().x };
			const float height1{ t1.get_scale().y }, height2{ t2.get_scale().y };

			// Se crea los rectangulos a partir del transform, altura y anchura
			Rectangle a{ vec2(t1.get_position().x, t1.get_position().y), width1, height1, t1.get_rotation().z },
					  b{ vec2(t2.get_position().x, t2.get_position().y), width2, height2, t2.get_rotation().z };

			// Se crea los poligonos a partir de los vertices de los rectangulos
			std::array<vec2, 4> corners{ a.get_corners() };
			polygon p1{ { corners[0], corners[1], corners[2], corners[3] }, vec2(t1.get_position().x, t1.get_position().y) };
			corners = b.get_corners();
			polygon p2{ { corners[0], corners[1], corners[2], corners[3] }, vec2(t2.get_position().x, t2.get_position().y) };

			// A partir de los poligonos se comprueba si hay colision mediante el algoritmo SAT / Teorema del Eje de Separacion
			return ShapeOverlap_SAT(p1, p2);
		}

		// Constructor del rectangulo
		Rectangle::Rectangle(vec2 pos, float w, float h, float angle) : center_pos{ pos }, width{ w }, height{ h }, angle{ angle } {}


		std::array<engine::collision::vec2, 4> Rectangle::get_corners()
		{
			// Los vertices se inician como si el rectangulo no tuviera rotacion (A continuacion se anadira la rotacion de cada vertice)
			std::array<vec2, 4> corners 
			{
				vec2(center_pos.x - width, center_pos.y - height),
				vec2(center_pos.x - width, center_pos.y + height),
				vec2(center_pos.x + width, center_pos.y + height),
				vec2(center_pos.x + width, center_pos.y - height)
			};

			glm::vec2 center{ center_pos.x, center_pos.y };

			// Se aplica la rotacion del rectangulo a los vertices
			for (size_t i{ 0 }; i != 4; ++i)
			{
				// Se translada el vertice al punto de origen
				vec2 origin{ corners[i].x - center_pos.x, corners[i].y - center_pos.y }; 

				// Ahora se aplica la rotacion (en el punto de origen)
				glm::vec2 rotated{ origin.x * cos(angle) - origin.y * sin(angle), origin.x * sin(angle) + origin.y * cos(angle) }; 

				// Se translada de vuelta a su posicion (teniendo ya con la rotacion)
				glm::vec2 trans{ rotated + center };
				corners[i].x = trans.x; 
				corners[i].y = trans.y;
			}

			return corners;
		}
	}
}

