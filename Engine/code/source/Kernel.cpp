/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */


#include <Kernel.hpp>
#include <Task.hpp>
#include <Timer.hpp>
#include <string>
#include <SDL.h>
#include <Message.hpp>
#include <algorithm>
#include <internal/id.hpp>
#include <Scene.hpp>

namespace engine
{
	Kernel::Kernel(Scene& scene) : scene{ scene } 
	{
	}
	void Kernel::handle(Message& message)
	{
		if (message.get_id() == ID(Exit Event)) exit();
	}

	void Kernel::start_observing_events()
	{
		// Se suscribe el kernel a los mensajes de eventos de exit.
		scene.get_dispatcher().add(ID(Exit Event), this); // Recibira notificaciones de todo mensaje con ID: Exit Event
	}

	void Kernel::exit()
	{
		forced_exit = true;
		stop();
	}

	void Kernel::pause()
	{
		paused = true;
	}

	void Kernel::stop()
	{
		// TODO: El stop deberia hacer que acabe todo el juego. No solo el nivel actual.
		ended = true;
	}

	void Kernel::resume()
	{
		paused = false;
	}

	void Kernel::add_task(Task *task_ptr)
	{
		if (task_ptr->is_consumible())
			consumible_task_ptr_vec.push_back(task_ptr);
		else
		recurrent_task_ptr_set.insert(task_ptr);
	}

	void Kernel::init()
	{
		start_observing_events();
	}

	// TODO: Hacer que las tareas usen un sistema multithread

	void Kernel::run() const
	{
		const float max_delta{ 1.0f / 80.0f };
		for (Task* const task_ptr : recurrent_task_ptr_set) task_ptr->initialize();

		float delta = max_delta; 
		do
		{
			Timer timer;
			if (!paused)
			{
				for (Task* const task_ptr : consumible_task_ptr_vec) task_ptr->run(delta);
				for (Task* const task_ptr : recurrent_task_ptr_set) task_ptr->run(delta);
			}
			//SDL_Log(std::to_string(1.0 / delta).c_str());
			delta = std::min((float)timer.get_elapsed(), max_delta);

		} while (!ended);

		// finalize(); TODO: Crear finalize 
	}

	bool Kernel::Task_ptr_cmp::operator()(const Task* lhs, const Task* rhs) const
	{
		return lhs->operator<(*rhs);
	}
}