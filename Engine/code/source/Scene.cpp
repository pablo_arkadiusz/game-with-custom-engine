/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Scene.hpp>
#include <SDL.h>
#include <Input_task.hpp>
#include <Audio_task.hpp>
#include <Game_object.hpp>
#include <algorithm>
#include <Scene_parser.hpp>
#include <Mono_behaviour.hpp>
#include <iterator>
#include <Scene_director.hpp>

namespace engine
{
	Render_system& Scene::get_render_system()
	{
		return render_system;
	}

	Collision_system& Scene::get_collision_system()
	{
		return collision_system;
	}

	Input_task& Scene::get_input_system()
	{
		return input_system;
	}

	Window& Scene::get_window() const
	{
		return window;
	}

	Audio_task& Scene::get_audio_task()
	{
		return audio_task;
	}

	Message_dispatcher& Scene::get_dispatcher()
	{
		return message_dispatcher;
	}

	Scene::Scene(Window& window) : window{ window }
	{
		// TODO: hacer que solo se activen si existen componentes que necesiten de esos sistemas
		init_collision();
		init_render();
		init_input();
		init_update();
		init_audio();
		init_dispatcher();
		init_kernel();
	}

	void Scene::add_game_object(std::shared_ptr<Game_object> game_object_ptr)
	{
		// Se anade el gameobject al mapa de nombres y de id's
		id_map.insert(std::make_pair(game_object_ptr->get_instance_id(), game_object_ptr));
		name_map.insert(std::make_pair(game_object_ptr->get_name(), game_object_ptr));
	}

	std::shared_ptr<Game_object> Scene::find_gameobject(const std::string & name)
	{
		name_list_type::iterator it = name_map.find(name);
		return it == name_map.cend() ? std::shared_ptr<Game_object>(nullptr) : it->second;
	}

	std::vector<std::shared_ptr<Game_object>> Scene::find_gameobjects(const std::string& name)
	{
		iter_pair range = name_map.equal_range(name);
		std::vector<std::shared_ptr<Game_object>> vec;
		std::transform(range.first, range.second, std::back_inserter(vec), [](name_pair element) {return element.second; });

		return vec;
	}

	std::shared_ptr<Game_object> Scene::find_gameobject_by_id(size_t id)
	{
		id_list_type::iterator it = id_map.find(id);
		return it == id_map.cend() ? std::shared_ptr<Game_object>() : it->second;
	}

	void Scene::init_update()
	{
		//SDL_Log("init update");
		kernel.add_task(&update_task);
	}

	void Scene::init_render()
	{
		//SDL_Log("Init render");
		kernel.add_task(&render_system);
	}

	void Scene::init_input()
	{
		//SDL_Log("Init render");
		kernel.add_task(&input_system);
	}

	void Scene::init_collision()
	{
		kernel.add_task(&collision_system);
	}

	void Scene::init_audio()
	{
		kernel.add_task(&audio_task);
	}

	void Scene::init_dispatcher()
	{
		kernel.add_task(&message_dispatcher);
	}

	void Scene::init_kernel()
	{
		kernel.init();
	}


	void Scene::load_scene_xml(const std::string& path, std::unordered_map<std::string, Mono_behaviour*> &mapping)
	{
		Scene_parser parser;
		parser.parse(path, *this, mapping);

	}

	void Scene::load_input_xml(const std::string& path)
	{
		input_system.load_from_xml(path);
	}

	void Scene::print(std::string s)
	{
		SDL_Log(s.c_str());
	}

	void Scene::run()
	{
		kernel.run();

		Scene_director& director{ Scene_director::get_instance() };
		director.set_state(!kernel.is_exit_state());
		
	}

	void Scene::stop()
	{
		kernel.stop();
	}
}
