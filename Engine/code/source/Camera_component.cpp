/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Camera_component.hpp>
#include <Camera.hpp>
#include <Scene.hpp>
#include <SDL.h>

namespace engine
{
	std::shared_ptr<glt::Node> Camera_component::get_render_node() const { return camera_ptr; }

	Camera_component::Camera_component() : Render_component("camera"), camera_ptr{ new glt::Camera(20.f, 1.f, 50.f, 1.f) }{}
}