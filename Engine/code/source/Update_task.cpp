/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */


#include <Update_task.hpp>
#include <SDL.h>
#include <string>
#include <Game_object.hpp>
#include <Mono_behaviour.hpp>

namespace engine
{

	Update_task::Update_task(ptr_list_type& game_objects_ptrs) : object_ptr_list{ game_objects_ptrs }, Task(10) {}

	void Update_task::initialize()
	{
		for (pair_type& it : object_ptr_list)
		{
			std::string name{ it.second->get_name() };

			// Se llama el metodo start de todos los monobehaviours activos
			Mono_behaviour* mono{ it.second->get_mono_ptr() };
			if (mono)
				if(mono->enabled) mono->start();		
		}
	}

	void Update_task::run(float delta_time)
	{
		for (pair_type& it : object_ptr_list) 
		{
			// Se llama el metodo update de todos los monobehaviours activos
			Mono_behaviour* mono{ it.second->get_mono_ptr() };
			if (mono)
				if(mono->enabled) mono->update(delta_time);
		}
	}

} 