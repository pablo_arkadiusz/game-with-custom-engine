/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Input_task.hpp>
#include <SDL.h>
#include <Scene.hpp>
#include <Message.hpp>
#include <Input_parser.hpp>
#include <internal/id.hpp>

namespace engine
{
	void Input_task::add_input_action(const Message& event_message, const std::string& name)
	{
		input_mapping.insert(std::make_pair(event_message, name));
	}

	Input_task::Input_task(Scene& s) : dispatcher{ s.get_dispatcher() }, Task(9, false)
	{}

	void Input_task::load_from_xml(const std::string& path)
	{
		Input_parser parser;
		if (!parser.parse(path, *this))
			SDL_LogError(SDL_LOG_CATEGORY_INPUT, "Couldn't parse input from xml");

	}

	void Input_task::run(float delta_time)
	{
		Message message(ID(Input Event));
		while (pool_event(message))
		{
			input_map_type::iterator iter{ input_mapping.find(message) };

			// Se comprueba si el mensaje del pool event es una accion de input
			// Y en caso de que lo sea se envia un mensaje con el id "Input Action" y su nombre. 
			if (iter != input_mapping.cend())
			{
				Message input_message(ID(Input Action)); // Se asigna el id de este evento de accion de input
				input_message.data[ID(Action Name)] = iter->second.c_str(); // Se asigna el nombre de este evento de accion de input Ej: "Jump" o "Move Forward"
				dispatcher.send(input_message); // Se envia el mensaje
			}
			// Ademas de enviar el mensaje de evento de accion de input tambien se envia 
			// el evento de input aunque no coincida con ninguna accion de input.
			// Para que el controlador sepa en todo momento que teclas estan pulsadas
			dispatcher.send(message);
		}
	}


	bool Input_task::pool_event(Message& event)
	{
		SDL_Event sdl_event;

		if (SDL_PollEvent(&sdl_event) > 0)
			switch (sdl_event.type)
			{
			case SDL_QUIT:

				event.change_id(ID(Exit Event));
				//event.data[ID(Key Type)] = Input_event::Type::QUIT;
				return true;

			case SDL_KEYDOWN:
				event.data[ID(Key Type)] = Input_event::KEY_DOWN;
				event.data[ID(Key Code)] = Keyboard::translate_sdl_key_code(sdl_event.key.keysym.sym);
				return true;

			case SDL_KEYUP:
				event.data[ID(Key Type)] = Input_event::KEY_UP;
				event.data[ID(Key Code)] = Keyboard::translate_sdl_key_code(sdl_event.key.keysym.sym);
				return true;
			}

		return false;
	}

}