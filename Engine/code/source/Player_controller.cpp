/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Player_controller.hpp>
#include <Input_event.hpp>
#include <SDL.h>
#include <IObserver.hpp>
#include <Input_task.hpp>
#include <Message.hpp>
#include <internal/id.hpp>

namespace engine
{

	void Player_controller::start_observing_events()
	{
		// Se suscribe el player controller a los mensajes de eventos de input y eventos de acciones input
		// Los eventos de input es cualquier tecla presionada. Y las acciones input son para las acciones predefinidas como por ejemplo Jump o Move Forward
		get_game_object().get_scene().get_dispatcher().add(ID(Input Event), this); // Recibira notificaciones de todo mensaje con ID: Input Event
		get_game_object().get_scene().get_dispatcher().add(ID(Input Action), this);// Recibira notificaciones de todo mensaje con ID: Input Action
	}

	void Player_controller::update_input_info(Message & message)
	{
		// Se comprueba si el mensaje tiene un valor de key type (En teoria todos lo tienen, pero por si acaso)
		Input_event::Type* input_type{ message.read<Input_event::Type>(ID(Key Type)) };
		if (!input_type) return;


		// Guarda constancia de cada una de las teclas que se ha pulsado
		if (*input_type == Input_event::Type::KEY_DOWN)
			switch (*message.read<int>(ID(Key Code)))
			{
			case Keyboard::KEY_Q:	player_input.Q = true; break;	case Keyboard::KEY_W:	player_input.W = true; break;
			case Keyboard::KEY_E:	player_input.E = true; break;	case Keyboard::KEY_R:	player_input.R = true; break;
			case Keyboard::KEY_T:	player_input.T = true; break;	case Keyboard::KEY_Y:	player_input.Y = true; break;
			case Keyboard::KEY_U:	player_input.U = true; break;	case Keyboard::KEY_I:	player_input.I = true; break;
			case Keyboard::KEY_O:	player_input.O = true; break;	case Keyboard::KEY_P:	player_input.P = true; break;
			case Keyboard::KEY_A:	player_input.A = true; break;	case Keyboard::KEY_S:	player_input.S = true; break;
			case Keyboard::KEY_D:	player_input.D = true; break;	case Keyboard::KEY_F:	player_input.F = true; break;
			case Keyboard::KEY_G:	player_input.G = true; break;	case Keyboard::KEY_H:	player_input.H = true; break;
			case Keyboard::KEY_J:	player_input.J = true; break;	case Keyboard::KEY_K:	player_input.K = true; break;
			case Keyboard::KEY_L:	player_input.L = true; break;	case Keyboard::KEY_Z:	player_input.Z = true; break;
			case Keyboard::KEY_X:	player_input.X = true; break;	case Keyboard::KEY_C:	player_input.C = true; break;
			case Keyboard::KEY_V:	player_input.V = true; break;	case Keyboard::KEY_B:	player_input.B = true; break;
			case Keyboard::KEY_N:	player_input.N = true; break;	case Keyboard::KEY_M:	player_input.M = true; break;
			default:break;
			}

		// Guarda constancia de cada una de las teclas que se ha dejado de pulsar
		else if (*input_type == Input_event::Type::KEY_UP)
			switch (*message.read<int>(ID(Key Code)))
			{
			case Keyboard::KEY_Q:	player_input.Q = false; break;	case Keyboard::KEY_W:	player_input.W = false; break;
			case Keyboard::KEY_E:	player_input.E = false; break;	case Keyboard::KEY_R:	player_input.R = false; break;
			case Keyboard::KEY_T:	player_input.T = false; break;	case Keyboard::KEY_Y:	player_input.Y = false; break;
			case Keyboard::KEY_U:	player_input.U = false; break;	case Keyboard::KEY_I:	player_input.I = false; break;
			case Keyboard::KEY_O:	player_input.O = false; break;	case Keyboard::KEY_P:	player_input.P = false; break;
			case Keyboard::KEY_A:	player_input.A = false; break;	case Keyboard::KEY_S:	player_input.S = false; break;
			case Keyboard::KEY_D:	player_input.D = false; break;	case Keyboard::KEY_F:	player_input.F = false; break;
			case Keyboard::KEY_G:	player_input.G = false; break;	case Keyboard::KEY_H:	player_input.H = false; break;
			case Keyboard::KEY_J:	player_input.J = false; break;	case Keyboard::KEY_K:	player_input.K = false; break;
			case Keyboard::KEY_L:	player_input.L = false; break;	case Keyboard::KEY_Z:	player_input.Z = false; break;
			case Keyboard::KEY_X:	player_input.X = false; break;	case Keyboard::KEY_C:	player_input.C = false; break;
			case Keyboard::KEY_V:	player_input.V = false; break;	case Keyboard::KEY_B:	player_input.B = false; break;
			case Keyboard::KEY_N:	player_input.N = false; break;	case Keyboard::KEY_M:	player_input.M = false; break;
			default:break;
			}
	}

	void Player_controller::on_added_to_gameobject(Game_object& game_object)
	{
		Component::on_added_to_gameobject(game_object);	
		start_observing_events(); // Suscripcion al listener (Message_dispatcher)
	}

	void Player_controller::handle(Message& message)
	{
		switch (message.get_id())
		{
		case ID(Input Event): // En caso de que se reciba un evento de input normal (Pulsar una tecla cualquiera)
			update_input_info(message);
			break;
		
		case ID(Input Action): // En caso de que se reciba un evento de input que es una accion predefinida, ej: Jump, Move Forward, etc

			// Se obtiene el nombre de la accion  ej: Jump, Move Forward, etc
			std::string action{ *message.read<std::string>(ID(Action Name)) }; 

			// Se comprueba si hay algun metodo bindeado a esta accion y en caso afirmativo se ejecuta
			funcion_map::iterator it{ actions.find(action) };
			if (it != actions.cend()) it->second();	
			break;

		}
	
	}
	
}