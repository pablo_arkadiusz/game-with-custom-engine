/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <unordered_map>
#include <Cube.hpp>
#include <Model.hpp>
#include <Light.hpp>
#include <Render_Node.hpp>
#include <Window.hpp>
#include <Render_system.hpp>
#include <Scene.hpp>
#include <SDL.h>
#include <cassert>
#include <Transform.hpp>
#include <Game_object.hpp>

namespace engine
{
	// Destructor explicito necesario para que compile el unique_ptr(render_node_ptr) de un tipo que se ha puesto como declaracion adelantada
	Render_system::~Render_system() = default; 

	Render_system::Render_system(Scene& scene) : Task(8), scene{ scene }, render_node_ptr{ std::make_unique<glt::Render_Node>() } {}

	void Render_system::add(Render_component *comp)
	{
		components_to_render.push_back(comp);

		// Se actualiza en nodo render con el Nodo del componente para renderizarlo
		render_node_ptr->add(comp->get_name(), comp->get_render_node());
	}

	void Render_system::initialize()
	{
		//for (Render_component* comp : components_to_render)
			//render_node_ptr->add(comp->get_name(), comp->get_render_node());
	}

	void Render_system::run(float delta_time)
	{
		render();
	}

	void Render_system::render()
	{
		Window& window{ scene.get_window() };
		window.clear();

		// Los componentes actualizan las transformaciones en las que se renderizaran (translacion, rotacion y escala)
		for (Component* comp : components_to_render)
		{		
			glt::Node* node{ render_node_ptr->get(comp->get_name()) };
			Transform &t{ comp->get_game_object().get_transform() };
			node->set_transformation(t.get_matrix());	
		}

		// Se ajusta el ratio por si ha cambiado el tamano de la ventana
		GLsizei width = GLsizei(window.get_width());
		GLsizei height = GLsizei(window.get_height());
		render_node_ptr->get_active_camera()->set_aspect_ratio(float(width) / height);
		glViewport(0, 0, width, height);

		// Se renderiza la escena y se intercambian los buffers de la ventana para hacer visible lo que se ha renderizado
		render_node_ptr->render();
		window.swap_buffers();
	}
}