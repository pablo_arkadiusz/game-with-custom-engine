/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Audio_source.hpp>
#include <Audio_task.hpp>
#include <Game_object.hpp>
#include <Scene.hpp>
#include <SDL_mixer.h>

namespace engine
{

	Audio_source::Audio_source(): Component ("Audio Source"){}

	Mix_Chunk *Audio_source::load_audio(const std::string &path)
	{
		Mix_Chunk* audio = nullptr;
		audio = Mix_LoadWAV(path.c_str());
		return audio;
	}

	_Mix_Music* Audio_source::load_music(const std::string& path)
	{
		_Mix_Music* music = nullptr;
		music = Mix_LoadMUS(path.c_str());
		return music;
	}



	void Audio_source::play_sound(Mix_Chunk* chunk)
	{
		audio_task_ptr->add_chunk(chunk);
	}

	void Audio_source::play_music(Mix_Music* music)
	{
		audio_task_ptr->add_music(music);
	}

	void Audio_source::on_added_to_gameobject(Game_object& game_object)
	{
		Component::on_added_to_gameobject(game_object);
		audio_task_ptr = &get_game_object().get_scene().get_audio_task();
	}

}
