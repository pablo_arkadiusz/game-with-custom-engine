/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Timer.hpp>

double Timer::get_elapsed()
{
	const long long start{ std::chrono::time_point_cast<micro_sec>(start_time).time_since_epoch().count() };
	const long long end{ std::chrono::time_point_cast<micro_sec>(clock::now()).time_since_epoch().count() };

	const long long us_duration{ end - start };
	const double sec_duration{ us_duration * 0.000001 };
	return sec_duration;
}

void Timer::stop()
{
	const long long start{ std::chrono::time_point_cast<micro_sec>(start_time).time_since_epoch().count() };
	const long long end{ std::chrono::time_point_cast<micro_sec>(clock::now()).time_since_epoch().count() };

	const long long us_duration{ end - start };
	const double ms_duration{ us_duration * 0.001 };

	std::cout << std::endl << "============= T I M E R -> ";
	std::cout << us_duration << " us (" << ms_duration << "  ms)" << std::endl;
}
