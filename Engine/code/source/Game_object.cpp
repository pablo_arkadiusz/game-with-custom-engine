/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */


#include <Game_object.hpp>
#include <string>
#include <Graphic_component.hpp>
#include <SDL.h>
#include <Scene.hpp>


namespace engine
{
	size_t Game_object::objects_amount{ 0 };

	Mono_behaviour* Game_object::get_mono_ptr()
	{
		return monobehaviour_ptr;
	}

	Game_object::Game_object(Scene& scene, const std::string &_name, std::function<void(Game_object*)> deleter) : scene{ scene }, name{ _name }, id{objects_amount++}
	{
		// Se crea el shared pointer a este objeto con el deleter que se encargara de destruirlo.
		std::shared_ptr<Game_object> shared_ptr{ this, deleter };
		scene.add_game_object(shared_ptr);
	}

	void Game_object::add_tag(const std::string &tag)
	{
		tags[tag] = true;
	}

	bool Game_object::check_tags(const std::initializer_list<std::string> &_tags) const
	{
		for (const std::string& tag : _tags)
			if (tags.find(tag) != tags.end()) return true;	
		return false;
	}

	size_t Game_object::get_instance_id() const
	{
		return id;
	}

	std::string Game_object::get_name() const
	{
		return name; 
	}

	Scene& Game_object::get_scene() const
	{
		return scene;
	}

	Transform& Game_object::get_transform()
	{
		return transform;
	}

	void Game_object::set_monobehaviour(Mono_behaviour &monobehaviour)
	{
		monobehaviour_ptr = &monobehaviour;
	}

	void Game_object::add_component(Component &comp)
	{
		comp.on_added_to_gameobject(*this);	
		// Al pasarlo por referencia se asume que no hay que preocuparse por el destructor 
		// asique se anade el componente con un destructor que no hace nada
		components.insert(std::make_pair(comp.get_name(), std::shared_ptr<Component>(&comp, [](Component*) {})));
	}

	void Game_object::add_component(std::shared_ptr<Component> comp_ptr)
	{
		comp_ptr->on_added_to_gameobject(*this);
		components.insert(std::make_pair(comp_ptr->get_name(), comp_ptr));
	}

	std::shared_ptr<Component> Game_object::find_component(const std::string& comp_name)
	{
		std::unordered_map<std::string, std::shared_ptr<Component>>::iterator got = components.find(comp_name);
		return got == components.end() ? std::shared_ptr<Component>(nullptr) : got->second;
	}

	bool Game_object::has_component(const std::string& comp_name) const
	{
		return components.find(comp_name) == components.end();
	}
}
