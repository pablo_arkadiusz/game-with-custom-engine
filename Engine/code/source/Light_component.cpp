/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Light_component.hpp>
#include <Light.hpp>
#include <Scene.hpp>
#include <SDL.h>

namespace engine
{
	Light_component::Light_component() : Render_component("light"), light_ptr{ new glt::Light }
	{
		//light_ptr->set_color(glt::Vector3(0, 0, 255));	
	}

	std::shared_ptr<glt::Node> engine::Light_component::get_render_node() const
	{
		return light_ptr;
	}
}
