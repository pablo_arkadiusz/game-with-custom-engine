/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Scene_parser.hpp>
#include <fstream>
#include <sstream>
#include <Scene.hpp>
#include <Game_object.hpp>
#include <Mono_behaviour.hpp>
#include <Camera_component.hpp>
#include <Light_component.hpp>
#include <SDL.h>
#include <Engine_allocator.hpp>

using namespace std;
using namespace rapidxml;

namespace engine
{

	bool Scene_parser::parse(const std::string& anim_file_path, Scene &scene, std::unordered_map<std::string, Mono_behaviour*> &mapping)
	{
		// Se lee el contenido de todo el archivo en la cadena xml_file_contents:

		std::string xml_file_contents;

		std::ifstream reader(anim_file_path, ifstream::binary);

		if (!reader) return false;

		reader.seekg(0, ifstream::end);

		size_t file_size = size_t(reader.tellg());

		if (file_size == 0) return false;

		reader.seekg(0, ifstream::beg);

		xml_file_contents.resize(file_size);

		reader.read(&xml_file_contents.front(), file_size);

		if (reader.fail() || reader.bad()) return false;

		// Se parsea el contenido del archivo XML:

		xml_document< > document;

		document.parse< 0 >(const_cast<char*>(xml_file_contents.c_str()));

		// Se obtiene el tag ra�z (que deber�a ser <scene>) y se llama a la funci�n que lo interpretar�:

		xml_node< >* animations_node = document.first_node("Scene");

		if (!animations_node) return false;

		return parse_scene(animations_node, scene, mapping);
	}

	bool Scene_parser::parse_scene(xml_node< >* scene_node, Scene& scene, std::unordered_map<std::string, Mono_behaviour*> &mapping)
	{
		// Se recorren los nodos de MonoBehaviours
		for (xml_node< >* child = scene_node->first_node("Mono_behaviours")->first_node("Mono_behaviour"); child; child = child->next_sibling())
		{
			// Se comprueba si el monobehaviour encontrado esta en el mapping que se le paso a la escena
			const std::string key{ child->first_attribute("name")->value() };
			std::unordered_map<std::string, Mono_behaviour*>::const_iterator iter{ mapping.find(key) };

			if (iter == mapping.cend()) return false; // Si no existe entonces ha habido algun error

			// En caso de que si exista se parsea este monobehaviour (para ajustar su transform y anadir posibles componentes)
			Mono_behaviour *mono_behaviour{ iter->second };
			parse_mono_behaviour(child, *mono_behaviour);
		}


		for (xml_node< >* child = scene_node->first_node("Game_objects")->first_node("Game_object"); child; child = child->next_sibling())
		{
			// Se aloja el objeto con el allocator del engine
			Engine_allocator& alloc{ Engine_allocator::get_instance() };
			// No se especifica el destructor como parametro ya que el propio allocator se encarga de ponerlo
			Game_object* entity( alloc.alloc_gameobject(scene, child->first_attribute("name")->value()) );
			if (!parse_entity(child, *entity)) return false;
		}

		return true;
	}

	bool Scene_parser::parse_entity(rapidxml::xml_node<>* entity_node, Game_object& entity)
	{
		// Se parsea el componente transform de al entidad
		if (!parse_transform(entity_node->first_node("Transform"), entity.get_transform())) return false;

		// Se parsean el resto de componentes de la entidad
		for (xml_node< >* child = entity_node->first_node("Components"); child; child = child->next_sibling())
			if (!parse_component(child, entity)) return false;
		return true;
	}

	bool Scene_parser::parse_transform(rapidxml::xml_node<>* transform_node, Transform& transform)
	{	
		// Necesita tener al menos un atributo de posicion, rotacion o de escala
		if (!transform_node->first_attribute("position") && !(transform_node->first_attribute("rotation")) && !(transform_node->first_attribute("scale")))
			return false;
			
		glm::vec3 val;	
		std::string segment;
	
		// Se parsea la posicion
		if (transform_node->first_attribute("position"))
		{
			std::istringstream test(transform_node->first_attribute("position")->value());

			// Se obtiene los valores x, y, z de la posicion
			test >> segment;
			val.x = std::stof(segment);
			test >> segment;
			val.y = std::stof(segment);
			test >> segment;
			val.z = std::stof(segment);

			transform.set_pos(val);		
		}

		// Se parsea la rotacion
		if (transform_node->first_attribute("rotation"))
		{
			std::istringstream test(transform_node->first_attribute("rotation")->value());

			// Se obtiene los valores x, y, z de la rotacion
			test >> segment;
			val.x = std::stof(segment);
			test >> segment;
			val.y = std::stof(segment);
			test >> segment;
			val.z = std::stof(segment);

			transform.set_rotation(val);
		}

		// Se parsea la escala
		if (transform_node->first_attribute("scale"))
		{
			std::istringstream test(transform_node->first_attribute("scale")->value());

			// Se obtiene los valores x, y, z de la escala
			test >> segment;
			val.x = std::stof(segment);
			test >> segment;
			val.y = std::stof(segment);
			test >> segment;
			val.z = std::stof(segment);

			transform.scale(val);
		}
		return true;
	}

	bool Scene_parser::parse_mono_behaviour(rapidxml::xml_node<>* mono_node, Mono_behaviour& mono_behaviour)
	{
	//	for (xml_node< >* child = mono_node->first_node("Transform"); child; child = child->next_sibling())
		//	if (!parse_transform(child, mono_behaviour.get_transform())) return false;
		// Se parsea el componente transform del component
		if(mono_node->first_node("Transform"))
			if (!parse_transform(mono_node->first_node("Transform"), mono_behaviour.get_transform())) return false;

		// Se parsean el resto de componentes del monobehaviour (de momento no se usa)
		/*for (xml_node< >* child = entity_node->first_node("Components"); child; child = child->next_sibling())
			if (!parse_component(child, entity)) return false;*/

		return true;
	}


	bool Scene_parser::parse_component(rapidxml::xml_node<>* component_node, Game_object& game_object)
	{
		//Se comprueba el nombre del componente y en funcion de eso de aloja el componente apropiado y se anade al gameobject
		for (xml_node< >* child = component_node->first_node("Component"); child; child = child->next_sibling())
			if (child->first_attribute("name"))
			{
				// La alojacion se realiza mediante el engine allocator,
				// por lo tanto es necesario especificar la forma de destruir el objeto en el shared_ptr
				Engine_allocator& alloc{ Engine_allocator::get_instance() };

				std::string component_name{ child->first_attribute("name")->value() };

				if (component_name == "Camera_component")
				{
					// deleter que indica como el componente tiene que ser liberado de memoria
					auto deleter{ [&alloc](Component* comp) { alloc.dealloc_camera_component(dynamic_cast<Camera_component*>(comp)); } };

					// Se anade el componente como un shared_ptr, que usara el deleter especificado
					game_object.add_component( std::shared_ptr<Component>(alloc.alloc_camera_component(), deleter));
				}

				else if (component_name == "Light_component")
				{
					auto deleter{ [&alloc](Component* comp) { alloc.dealloc_light_component(dynamic_cast<Light_component*>(comp)); } };
					game_object.add_component(std::shared_ptr<Component>(alloc.alloc_light_component(), deleter));
				}

				else if (component_name == "Cube_component")
				{
					auto deleter{ [&alloc](Component* comp) { alloc.dealloc_cube_component(dynamic_cast<Cube_component*>(comp)); } };
					game_object.add_component(std::shared_ptr<Component>(alloc.alloc_cube_component(), deleter));
				}

				else if (component_name == "Sphere_component")
				{
					auto deleter{ [&alloc](Component* comp) { alloc.dealloc_sphere_component(dynamic_cast<Sphere_component*>(comp)); } };
					game_object.add_component(std::shared_ptr<Component>(alloc.alloc_sphere_component(), deleter));
				}

				else if (component_name == "Box_collider_2d")
				{
					auto deleter{ [&alloc](Component* comp) { alloc.dealloc_box2d_component(dynamic_cast<Box_collider2d*>(comp)); } };
					game_object.add_component(std::shared_ptr<Component>(alloc.alloc_box2d_component(), deleter));
				}
				else return false;
			}	
		return true;
	}
}