/*
 * Autor: Pablo Arkadiusz Kalemba
 * Contacto: arkadiusz.pablo@gmail.com
 * Fecha: Enero 2021
 */

#include <Cube.hpp>
#include <Model.hpp>
#include <Light.hpp>
#include <Render_Node.hpp>
#include <Window.hpp>
#include <Render_system.hpp>
#include <Game_object.hpp>
#include <Model_Obj.hpp>
#include <Graphic_component.hpp>
#include <SDL.h>
#include <Scene.hpp>



namespace engine
{
	using namespace std;

	std::shared_ptr<glt::Node> Graphic_component::get_render_node() const
	{
		return model_ptr;
	}

	size_t Graphic_component::graphic_id{ 0 };

	//cube_ptr(new Model_Obj(string("../../../DEMO/assets/object.obj"))
	Graphic_component::Graphic_component(const std::string & type) : Render_component((type + std::to_string(++graphic_id)))
	{
		if (type == "Cube")
		{
			model_ptr.reset(new glt::Model);
			model_ptr->add(shared_ptr< Drawable >(new glt::Cube), Material::default_material());
		}
		else if (type == "Sphere")
		{
			model_ptr.reset(new Model_Obj(string("../../../DEMO/assets/sphere.obj")));
			model_ptr->add(std::shared_ptr<glt::Drawable>(model_ptr->get_pieces().begin()->drawable), Material::default_material());
		}
		else // Model
		{
			model_ptr.reset(new Model_Obj(type.c_str()));
			model_ptr->add(std::shared_ptr<glt::Drawable>(model_ptr->get_pieces().begin()->drawable), Material::default_material());
		}
		
		//SDL_Log("Construction of component of type: ");
		//SDL_Log(get_name().c_str());
	}

}